﻿using System.Threading.Tasks;
using Microsoft.Owin;

namespace Multitenancy.Owin
{
	public class TestMiddleware : OwinMiddleware
	{
		public TestMiddleware(OwinMiddleware next) : base(next)
		{
		}

		public override Task Invoke(IOwinContext context)
		{
			var tenant = context.Request.Environment["Tenant"];

			return this.Next.Invoke(context);
		}
	}
}