﻿using System.Web.Http;
using Common;
using Owin;

//[assembly: OwinStartup(typeof(Multitenancy.Owin.Startup))]

namespace Multitenancy.Owin
{
	public static class Startup
	{
		public static void Configuration(IAppBuilder builder)
		{
			builder.UseStaticFiles();
			builder.UseDefaultFiles();
			//rest goes here
			builder.UseWebApi(new HttpConfiguration());
			//etc

			builder.Use<OwinHostHeaderTenantIdentifierStrategy>();
			builder.Use<MultitenancyMiddleware>();
			builder.Use<TestMiddleware>();
		}
	}
}