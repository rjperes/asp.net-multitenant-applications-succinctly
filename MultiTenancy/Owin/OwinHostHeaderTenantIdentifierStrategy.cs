﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Common;
using Microsoft.Owin;

namespace Multitenancy.Owin
{
	public class OwinHostHeaderTenantIdentifierStrategy : OwinMiddleware
	{
		public OwinHostHeaderTenantIdentifierStrategy(OwinMiddleware next) : base(next)
		{
		}

		public override Task Invoke(IOwinContext context)
		{
			var headers = context.Request.Environment["owin.RequestHeaders"] as IDictionary<String, String[]>;

			context.Request.Environment["Tenant"] = TenantsConfiguration.GetTenants().Single(x => x.Name() == context.Request.Host.Value.Split(':').First().ToLower());
			headers["Host"] = new String[] { "abc.com" };

			var h = context.Request.Host;
			context.Request.Host = new HostString("abc.com");


			//context.Request.Environment["Tenant"] = context.Request.Host.Value.Split(':').First().ToLower();

			return this.Next.Invoke(context);
		}
	}
}