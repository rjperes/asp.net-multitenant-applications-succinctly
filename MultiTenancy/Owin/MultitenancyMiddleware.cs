﻿using System;
using System.Threading.Tasks;
using Common;
using Microsoft.Owin;

namespace Multitenancy.Owin
{
	public class MultitenancyMiddleware : OwinMiddleware
	{
		public MultitenancyMiddleware(OwinMiddleware next) : base(next)
		{
		}

		public override Task Invoke(IOwinContext context)
		{
			//context.Set<ITenantLocationStrategy>(typeof(ITenantLocationStrategy).FullName, new MefTenantLocationStrategy(typeof(Common.ContextRepository).Assembly));
			context.Set<IConfiguration>(String.Empty, AppSettingsConfiguration.Instance);

			//var tls = context.Get<ITenantLocationStrategy>(typeof(ITenantLocationStrategy).FullName);
			var cfg = context.Get<IConfiguration>(String.Empty);
			
			return this.Next.Invoke(context);
		}
	}
}