﻿using System;
using System.ServiceModel;
using System.ServiceModel.Web;

namespace Wcf
{
	[ServiceContract]
	public interface IDateService
	{

		[OperationContract]
		[WebGet(ResponseFormat = WebMessageFormat.Json)]
		DateTime GetDate();
	}
}
