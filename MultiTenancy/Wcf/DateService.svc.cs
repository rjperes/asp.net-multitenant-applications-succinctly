﻿using System;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Web;

namespace Wcf
{
	[AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Required)]
	public class DateService : IDateService
	{
		public DateTime GetDate()
		{
			

			var wcfCtx = OperationContext.Current;
			var httpCtx = HttpContext.Current;
			var webCtx = WebOperationContext.Current;

			var ident = new WcfHostHeaderTenantIdentification();
			var tenant = ident.GetCurrentTenant(null);

			var tenantCookies = HttpContext.Current.Request.Cookies.OfType<HttpCookie>().Where(x => x.Name.StartsWith(String.Concat(tenant, ":")));


			return DateTime.Now;
		}
	}
}
