﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Web;
using System.Web;
using System.Web.Routing;
using Common;

namespace Wcf
{
	public class WcfHostHeaderTenantIdentification : ITenantIdentifierStrategy
	{
		public String GetCurrentTenant(System.Web.Routing.RequestContext context)
		{
			var request = WebOperationContext.Current.IncomingRequest;
			var host = request.Headers["Host"];

			return host.Split(':').First().ToLower();
		}
	}
}