﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.Threading.Tasks;
using Common;
using Microsoft.Samples.Eventing;
using Wcf;

namespace Console
{
	class Program
	{
		static void Main(string[] args)
		{
			var RewriteProviderId = new Guid(MultiTenantEventSource.SourceId);

			using (var watcher = new EventTraceWatcher(MultiTenantEventSource.SourceName, new Guid(MultiTenantEventSource.SourceId)))
			{
				watcher.EventArrived += (sender, e) =>
				{
					System.Console.WriteLine();
				};

				watcher.Start();

				System.Console.ReadLine();
			} 

			var host = new WebServiceHost(typeof(DateService), new Uri("http://localhost"));
			host.AddServiceEndpoint(typeof(IDateService), new WebHttpBinding(), new Uri("http://localhost"));
			host.Open();


			System.Console.ReadLine();
		}
	}
}
