﻿using System;
using System.Collections.Generic;

namespace Model
{
	[Serializable]
	public class Customer
	{
		public Customer()
		{
			this.Orders = new HashSet<Order>();
		}
		public Int32 CustomerId { get; set; }
		public String Name { get; set; }
		public String Email { get; set; }
		public virtual ICollection<Order> Orders { get; protected set; }
	}
}
