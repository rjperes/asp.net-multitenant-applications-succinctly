﻿using System;
using System.Collections.Generic;

namespace Model
{
	[Serializable]
	public class Product
	{
		public Product()
		{
			this.OrderDetails = new HashSet<OrderDetail>();
		}
		public Int32 ProductId { get; set; }
		public String Name { get; set; }
		public Decimal Price { get; set; }
		public virtual ICollection<OrderDetail> OrderDetails { get; protected set; }
	}
}
