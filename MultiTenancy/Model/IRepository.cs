﻿using System;
using System.Linq;
using System.Linq.Expressions;

namespace Model
{
	public interface IRepository : IDisposable
	{
		T Find<T>(Object id);

		IQueryable<T> All<T>(params Expression<Func<T, Object>> [] expansions);

		void Delete(Object item);

		void Save(Object item);

		void Update(Object item);

		void Refresh(Object item);

		void SaveChanges();
	}
}
