﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Configuration;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Reflection;

namespace Model
{
	public sealed class DiscriminatorConvention : TypeAttributeConfigurationConvention<DiscriminatorAttribute>
	{
		private static readonly MethodInfo entityMethod = typeof(DbModelBuilder).GetMethod("Entity");

		private readonly DbModelBuilder modelBuilder;
		private readonly ISet<Type> types = new HashSet<Type>();

		public DiscriminatorConvention(DbModelBuilder modelBuilder)
		{
			this.modelBuilder = modelBuilder;
		}

		public override void Apply(ConventionTypeConfiguration configuration, DiscriminatorAttribute attribute)
		{
			if (this.types.Contains(configuration.ClrType) == true)
			{
				//if the type has already been processed, bail out
				return;
			}

			//add the type to the list of processed types
			this.types.Add(configuration.ClrType);

			dynamic entity = entityMethod.MakeGenericMethod(configuration.ClrType).Invoke(modelBuilder, null);

			Action<dynamic> action = arg =>
			{
				var valueConditionConfiguration = arg.Requires(attribute.DiscriminatorColumnName);
				var value = DiscriminatorAttribute.GetDiscriminatorValue(configuration.ClrType);
				var hasValueMethod = null as MethodInfo;

				if (value is String)
				{
					hasValueMethod = typeof(ValueConditionConfiguration).GetMethods().Single(m => (m.Name == "HasValue") && (m.IsGenericMethod == false));
				}
				else
				{
					hasValueMethod = typeof(ValueConditionConfiguration).GetMethods().Single(m => (m.Name == "HasValue") && (m.IsGenericMethod == true)).MakeGenericMethod(value.GetType());
				}

				hasValueMethod.Invoke(valueConditionConfiguration, new Object[] { value });
			};

			entity.Map(action);
		}
	}
}
