﻿using System.Data.Entity;

namespace Model
{
	public static class DbModelBuilderExtensions
	{
		public static DbModelBuilder ApplyDiscriminators(this DbModelBuilder modelBuilder)
		{
			modelBuilder.Conventions.Remove<DiscriminatorConvention>();
			modelBuilder.Conventions.Add(new DiscriminatorConvention(modelBuilder));

			return (modelBuilder);
		}
	}
}
