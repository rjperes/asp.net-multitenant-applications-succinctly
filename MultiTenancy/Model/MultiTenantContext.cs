﻿using System;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace Model
{
	public class MultiTenantContext : DbContext
	{
		static MultiTenantContext()
		{
			Database.SetInitializer(new DropCreateDatabaseAlways/*IfModelChanges*/<MultiTenantContext>());
		}

		public MultiTenantContext(String nameOrConnectionString) : base(nameOrConnectionString)
		{
		}

		protected override void OnModelCreating(DbModelBuilder modelBuilder)
		{
			modelBuilder.Conventions.Remove<PluralizingEntitySetNameConvention>();
			modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();

			modelBuilder.ApplyDiscriminators();
			//modelBuilder.Conventions.Add(new DiscriminatorConvention(modelBuilder));
			//modelBuilder.Entity<DataStore>().Map(m => m.Requires("Tenant").HasValue("abc.com"));

			base.OnModelCreating(modelBuilder);
		}

		public DbSet<DataStore> DataStores { get; set; }
		public DbSet<Product> Products { get; set; }
		public DbSet<Order> Orders { get; set; }
		public DbSet<Customer> Customers { get; set; }
	}
}
