﻿using System;

namespace Model
{
	[Discriminator("Tenant", "abc.com")]

	public class DataStore
	{
		public Int32 DataStoreId { get; set; }

		public String Data { get; set; }
	}
}
