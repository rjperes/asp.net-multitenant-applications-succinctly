﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration.Configuration;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace Model
{
	public sealed class MultiTenantSchemaConvention : TableAttributeConvention
	{
		public override void Apply(ConventionTypeConfiguration configuration, TableAttribute attribute)
		{
			attribute.Schema = "bla";

			base.Apply(configuration, attribute);
		}
	}
}
