﻿using System;

namespace Model
{
	[Serializable]
	public class OrderDetail
	{
		public Int32 OrderDetailId { get; set; }
		public virtual Order Order { get; set; }
		public virtual Product Product { get; set; }
		public Int32 Quantity { get; set; }
	}
}
