﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration.Configuration;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace Model
{
	public sealed class MultiTenantTableConvention : TableAttributeConvention
	{
		public override void Apply(ConventionTypeConfiguration configuration, TableAttribute attribute)
		{
			attribute = new TableAttribute("bla_" + attribute.Name) { Schema = attribute.Schema };

			base.Apply(configuration, attribute);
		}
	}
}
