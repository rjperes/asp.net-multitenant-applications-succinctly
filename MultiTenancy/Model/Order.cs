﻿using System;
using System.Collections.Generic;

namespace Model
{
	[Serializable]
	public class Order
	{
		public Order()
		{
			this.OrderDetails = new HashSet<OrderDetail>();
		}
		public Int32 OrderId { get; set; }
		public DateTime Date { get; set; }
		public virtual Customer Customer { get; set; }
		public virtual ICollection<OrderDetail> OrderDetails { get; protected set; }
	}
}
