﻿using System;
using System.Reflection;

namespace Model
{
	[Serializable]
	[AttributeUsage(AttributeTargets.Class, AllowMultiple = false, Inherited = true)]
	public sealed class DiscriminatorAttribute : Attribute
	{
		public const String DefaultDiscriminatorColumnName = "Tenant";

		public DiscriminatorAttribute(String discriminatorColumnName, Object discriminatorValue)
		{
			this.DiscriminatorColumnName = discriminatorColumnName ?? DefaultDiscriminatorColumnName;
			this.DiscriminatorValue = discriminatorValue;
		}

		public DiscriminatorAttribute(Object discriminatorValue) : this(DefaultDiscriminatorColumnName, discriminatorValue)
		{
		}

		public DiscriminatorAttribute(String discriminatorColumnName, Type type, String methodName) : this(discriminatorColumnName, null)
		{
			this.Type = type;
			this.MethodName = methodName;
		}

		public DiscriminatorAttribute(Type type, String methodName) : this(DefaultDiscriminatorColumnName, type, methodName)
		{
		}

		public String DiscriminatorColumnName { get; private set; }

		public Object DiscriminatorValue { get; private set; }

		public String MethodName { get; private set; }

		public Type Type { get; private set; }

		public static Object GetDiscriminatorValue(Type type)
		{
			var attribute = Attribute.GetCustomAttribute(type, typeof(DiscriminatorAttribute), true) as DiscriminatorAttribute;
			var value = attribute.DiscriminatorValue;

			if ((attribute.Type != null) && (String.IsNullOrWhiteSpace(attribute.MethodName) == false))
			{
				var method = attribute.Type.GetMethod(attribute.MethodName, BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Static | BindingFlags.Instance);

				if (method == null)
				{
					throw (new InvalidOperationException("Method does not exist."));
				}

				if ((method.ReturnType == typeof(void)) || (method.GetParameters().Length != 1) || (method.ContainsGenericParameters == true) || (method.GetParameters()[0].ParameterType != typeof(Type)))
				{
					throw (new InvalidOperationException("Invalid method signature: the method cannot be generic or void and must take a single parameter of type Type."));
				}

				if ((method.IsStatic == false) && (attribute.Type.IsAbstract == true))
				{
					throw (new InvalidOperationException("The type must be a non-abstract class in order to use an instance method."));
				}

				var instance = (method.IsStatic == false) ? null : Activator.CreateInstance(attribute.Type);

				value = method.Invoke(instance, new Object[] { type });
			}
			else if (attribute.DiscriminatorColumnName == null)
			{
				throw (new InvalidOperationException("The type or method name is invalid."));
			}

			return (value);
		}

		public override Boolean Equals(Object obj)
		{
			var other = obj as DiscriminatorAttribute;

			if (other == null)
			{
				return (false);
			}

			return ((this.DiscriminatorColumnName == other.DiscriminatorColumnName) && (Object.Equals(this.DiscriminatorValue, other.DiscriminatorValue) == true) && (this.Type == other.Type) && (this.MethodName == other.MethodName));
		}

		public override Int32 GetHashCode()
		{
			return (String.Concat(this.DiscriminatorColumnName, ":", this.DiscriminatorValue, ":", this.Type, ":", this.MethodName).GetHashCode());
		}
	}
}
