﻿using NHibernate.Mapping.ByCode.Conformist;

namespace Model
{
	public class DataStoreClassMapping : ClassMapping<DataStore>
	{
		public DataStoreClassMapping()
		{
			this.Filter("tenant", filter =>
				{
					filter.Condition("tenant = :tenant");
				});
			//rest goes here
		}
	}
}
