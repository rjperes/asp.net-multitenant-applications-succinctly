﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Routing;

namespace Common
{
	public sealed class SourceDomainTenantIdentifierStrategy : ITenantIdentifierStrategy
	{
		private readonly Dictionary<String, String> domains = new Dictionary<String, String>(StringComparer.InvariantCultureIgnoreCase);

		public SourceDomainTenantIdentifierStrategy Add(String domain, String name)
		{
			this.domains[domain.ToLower()] = name.ToLower();
			return (this);
		}

		public SourceDomainTenantIdentifierStrategy Add(String domain)
		{
			return (this.Add(domain, domain));
		}

		#region ITenantIdentifierStrategy Members

		public String GetCurrentTenant(RequestContext context)
		{
			var hostName = context.HttpContext.Request.UserHostName;
			var domainName = String.Join(".", hostName.Split('.').Skip(1)).ToLower();

			return (this.domains.Where(domain => domain.Key == domainName).Select(domain => domain.Value).FirstOrDefault());
		}

		#endregion
	}
}
