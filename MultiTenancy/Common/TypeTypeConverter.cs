﻿using System;
using System.ComponentModel;
using System.Globalization;

namespace Common
{
	public sealed class TypeTypeConverter : TypeConverter
	{
		public override Boolean CanConvertFrom(ITypeDescriptorContext context, Type sourceType)
		{
			return (sourceType == typeof (String));
		}

		public override Boolean CanConvertTo(ITypeDescriptorContext context, Type destinationType)
		{
			return (destinationType == typeof(Type));
		}

		public override Object ConvertFrom(ITypeDescriptorContext context, CultureInfo culture, Object value)
		{
			if (value is String)
			{
				return (Type.GetType(value.ToString(), false, true));
			}

			return (base.ConvertFrom(context, culture, value));
		}

		public override Object ConvertTo(ITypeDescriptorContext context, CultureInfo culture, Object value, Type destinationType)
		{
			if ((destinationType == typeof (Type)) && (value is String))
			{
				return (this.ConvertFrom(context, culture, value));
			}

			return (base.ConvertTo(context, culture, value, destinationType));
		}
	}
}
