﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition.Hosting;
using System.ComponentModel.Composition.Primitives;
using System.Linq;
using System.Reflection;
using Microsoft.Practices.ServiceLocation;
using Microsoft.Practices.Unity;

namespace Common
{
	public sealed class MefTenantLocationStrategy : ITenantLocationStrategy
	{
		private readonly ComposablePartCatalog catalog;

		public MefTenantLocationStrategy(params String [] paths)
		{
			this.catalog = new AggregateCatalog(paths.Select(path => new DirectoryCatalog(path)));
		}

		public MefTenantLocationStrategy(params Assembly [] assemblies)
		{
			this.catalog = new AggregateCatalog(assemblies.Select(asm => new AssemblyCatalog(asm)));
		}

		#region ITenantLocationStrategy Members

		public IDictionary<String, Type> GetTenants()
		{
			ServiceLocator.Current.GetInstance<IUnityContainer>().RegisterType<IServiceProvider>(new PerResolveLifetimeManager(), new InjectionFactory(x => new UnityServiceLocator(x.CreateChildContainer())));

			//get the default tenant
			var tenants = this.catalog.GetExports(new ImportDefinition (a => true, null, ImportCardinality.ZeroOrMore, false, false)).ToList();
			var defaultTenant = tenants.Where(x => x.Item2.Metadata.ContainsKey("Default")).SingleOrDefault();

			if (defaultTenant != null)
			{
				var isDefault = Convert.ToBoolean(defaultTenant.Item2.Metadata["Default"]);

				if (isDefault == true)
				{
					if (String.IsNullOrWhiteSpace(TenantsConfiguration.DefaultTenant) == true)
					{
						TenantsConfiguration.DefaultTenant = defaultTenant.Item2.ContractName;
					}
				}
			}

			return (this.catalog.GetExportedTypes<ITenantConfiguration>());
		}

		#endregion
	}
}
