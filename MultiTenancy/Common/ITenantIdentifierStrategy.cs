﻿using System;
using System.Web.Routing;

namespace Common
{
	public interface ITenantIdentifierStrategy
	{
		String GetCurrentTenant(RequestContext context);
	}
}
