﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Web;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.Logging;
using Microsoft.Practices.ServiceLocation;

namespace Common
{
	public static class TenantsConfiguration
	{
		public static String DefaultTenant { get; set; }
		public static ITenantLocationStrategy TenantLocationStrategy { get; set; }
		public static ITenantIdentifierStrategy TenantIdentifierStrategy { get; set; }

		public static void Initialize()
		{
			var tenants = GetTenants();
			InitializeTenants(tenants);
			CreateLogFactories(tenants);
			CreatePerformanceCounters(tenants);
		}

		private static void InitializeExceptionHandling()
		{
			AppDomain.CurrentDomain.UnhandledException += (sender, ex) =>
				{
					var tenant = TenantsConfiguration.GetCurrentTenant();

					if (tenant != null)
					{
						tenant.OnException(ex.ExceptionObject as Exception);
					}
				};
		}

		private static void InitializeTenants(IEnumerable<ITenantConfiguration> tenants)
		{
			foreach (var tenant in tenants)
			{
				tenant.OnInitialize();
			}
		}

		private static void CreateLogFactories(IEnumerable<ITenantConfiguration> tenants)
		{
			foreach (var tenant in tenants)
			{
				try
				{					
					var configurationSource = new FileConfigurationSource(tenant.Name() + ".config");
					//var configurationSource = ConfigurationSourceFactory.Create();
					var logWriterFactory = new LogWriterFactory(configurationSource);
					Logger.SetLogWriter(logWriterFactory.Create());
				}
				catch
				{
				}
			}
		}

		private static void CreatePerformanceCounters(IEnumerable<ITenantConfiguration> tenants)
		{
			if (PerformanceCounterCategory.Exists("Tenants") == true)
			{
				PerformanceCounterCategory.Delete("Tenants");
			}

			var counterCreationDataCollection = new CounterCreationDataCollection(tenants.Select(tenant => new CounterCreationData(tenant.Name(), String.Empty, PerformanceCounterType.NumberOfItems32)).ToArray());
			var category = PerformanceCounterCategory.Create("Tenants", "Tenants performance counters", PerformanceCounterCategoryType.MultiInstance, counterCreationDataCollection);

			foreach (var tenant in tenants)
			{
				foreach (var instance in tenant.Counters())
				{
					var counter = new PerformanceCounter(category.CategoryName, tenant.Name(), String.Concat(tenant.Name(), ": ", instance.InstanceName), false);
				}
			}
		}

		public static IEnumerable<ITenantConfiguration> GetTenants()
		{
			var tenants = ServiceLocator.Current.GetAllInstances<ITenantConfiguration>();
			return (tenants);
		}

		public static ITenantConfiguration GetCurrentTenant()
		{
			var tenantIdentifier = ServiceLocator.Current.GetInstance<ITenantIdentifierStrategy>();
			var tenantId = tenantIdentifier.GetCurrentTenant(HttpContext.Current.Request.RequestContext);
			return (ServiceLocator.Current.GetInstance<ITenantConfiguration>(tenantId));
		}

		public static class Services
		{
			public static IDiagnostics Diagnostics
			{
				get
				{
					return (ServiceLocator.Current.GetInstance<IDiagnostics>());
				}
			}

			public static ILog Log
			{
				get
				{
					return (ServiceLocator.Current.GetInstance<ILog>());
				}
			}

			public static IConfiguration Configuration
			{
				get
				{
					return (ServiceLocator.Current.GetInstance<IConfiguration>());
				}
			}

			public static ICache Cache
			{
				get
				{
					return (ServiceLocator.Current.GetInstance<ICache>());
				}
			}
		}

		public static class Identifiers
		{
			public static readonly HostHeaderTenantIdentifierStrategy HostHeader = new HostHeaderTenantIdentifierStrategy();

			public static SourceDomainTenantIdentifierStrategy SourceDomain()
			{
				return (new SourceDomainTenantIdentifierStrategy());
			}

			public static SourceIPTenantIdentifierStrategy SourceIP()
			{
				return (new SourceIPTenantIdentifierStrategy());
			}

			public static QueryStringTenantIdentifierStrategy QueryString(String queryStringParameter = QueryStringTenantIdentifierStrategy.DefaultTenantKey)
			{
				return (new QueryStringTenantIdentifierStrategy(queryStringParameter));
			}
		}
		//r
		public static class Locations
		{
			public static ServiceLocatorTenantLocationStrategy ServiceLocator()
			{
				return (new ServiceLocatorTenantLocationStrategy());
			}

			public static XmlTenantLocationStrategy Xml()
			{
				return (XmlTenantLocationStrategy.Instance);
			}

			public static MefTenantLocationStrategy Mef(params Assembly[] assemblies)
			{
				return (new MefTenantLocationStrategy(assemblies));
			}

			public static DictionaryTenantLocationStrategy Dictionary(IDictionary<String, Type> tenants)
			{
				return (new DictionaryTenantLocationStrategy(tenants));
			}
		}
	}
}
