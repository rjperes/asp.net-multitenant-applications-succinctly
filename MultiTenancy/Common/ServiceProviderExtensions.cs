﻿using System;

namespace Common
{
	public static class ServiceProviderExtensions
	{
		public static T GetService<T>(this IServiceProvider serviceProvider)
		{
			var service = default(T);

			try
			{
				service = (T) serviceProvider.GetService(typeof (T));
			}
			catch
			{
			}

			return (service);
		}
	}
}
