﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Practices.ServiceLocation;

namespace Common
{
	public sealed class ServiceLocatorTenantLocationStrategy : ITenantLocationStrategy
	{
		#region ITenantLocationStrategy Members

		public IDictionary<String, Type> GetTenants()
		{
			return (ServiceLocator.Current.GetAllInstances<ITenantConfiguration>().ToDictionary(x => x.Name(), x => x.GetType()));
		}

		#endregion
	}
}
