﻿using System;

namespace Common
{
	[Serializable]
	[AttributeUsage(AttributeTargets.Class, AllowMultiple = false, Inherited = true)]
	public sealed class TenantMasterPageAttribute : TenantAttribute
	{
		public TenantMasterPageAttribute(String masterPage)
		{
			this.MasterPage = masterPage;
		}

		public String MasterPage { get; private set; }
	}
}
