﻿using System;
using System.Configuration;

namespace Common
{
	[Serializable]
	public sealed class TenantsSection : ConfigurationSection
	{
		public static readonly TenantsSection Section = ConfigurationManager.GetSection("tenants") as TenantsSection;

		[ConfigurationProperty("default", IsRequired = false, DefaultValue = "")]
		public String Default
		{
			get
			{
				return base["default"] as String;
			}
			set
			{
				base["default"] = value;
			}
		}

		[ConfigurationProperty("", IsDefaultCollection = true, IsRequired = true)]
		public TenantsElementCollection Tenants
		{
			get
			{
				return (base[String.Empty] as TenantsElementCollection);
			}
		}
	}
}
