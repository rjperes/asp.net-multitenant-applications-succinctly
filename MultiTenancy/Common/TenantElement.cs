﻿using System;
using System.ComponentModel;
using System.Configuration;

namespace Common
{
	[Serializable]
	public sealed class TenantElement : ConfigurationElement
	{
		[ConfigurationProperty("name", IsKey = true, IsRequired = true)]
		//[StringValidator(MinLength = 2)]
		public String Name
		{
			get
			{
				return (this["name"] as String);
			}
			set
			{
				this["name"] = value;
			}
		}

		[ConfigurationProperty("type", IsKey = true, IsRequired = true)]
		[TypeConverter(typeof(TypeTypeConverter))]
		public Type Type
		{
			get
			{
				return (this["type"] as Type);
			}
			set
			{
				this["type"] = value;
			}
		}

		[ConfigurationProperty("theme", DefaultValue = "")]
		public String Theme
		{
			get
			{
				return (this["theme"] as String);
			}
			set
			{
				this["theme"] = value;
			}
		}

		[ConfigurationProperty("masterPage", DefaultValue = "")]
		public String MasterPage
		{
			get
			{
				return (this["masterPage"] as String);
			}
			set
			{
				this["masterPage"] = value;
			}
		}

		[ConfigurationProperty("services")]
		public ServicesElementCollection Services
		{
			get
			{
				return (base["services"] as ServicesElementCollection);
			}
		}
	}
}