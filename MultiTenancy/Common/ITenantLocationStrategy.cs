﻿using System;
using System.Collections.Generic;

namespace Common
{
	public interface ITenantLocationStrategy
	{
		IDictionary<String, Type> GetTenants();
	}
}
