﻿using System;

namespace Common
{
	[Serializable]
	public class MultiTenantEventArgs : EventArgs
	{
		public MultiTenantEventArgs(MultiTenantEventProvider provider, MultiTenantWebEvent evt)
		{
			this.Provider = provider;
			this.Event = evt;
		}

		public MultiTenantEventProvider Provider { get; private set; }

		public MultiTenantWebEvent Event { get; private set; }
	}
}
