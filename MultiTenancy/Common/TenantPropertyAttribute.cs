﻿using System;

namespace Common
{
	[Serializable]
	[AttributeUsage(AttributeTargets.Class, AllowMultiple = true, Inherited = true)]
	public sealed class TenantPropertyAttribute : TenantAttribute
	{
		public TenantPropertyAttribute(String propertyName, Object propertyValue)
		{
			this.PropertyName = propertyName;
			this.PropertyValue = propertyValue;
		}

		public String PropertyName { get; private set; }
		public Object PropertyValue { get; private set; }
	}
}
