﻿using System;

namespace Common
{
	[Serializable]
	[AttributeUsage(AttributeTargets.Class, AllowMultiple = true, Inherited = true)]
	public sealed class TenantServiceAttribute : TenantAttribute
	{
		public TenantServiceAttribute(Type serviceType, String key = "", Type implementationType = null)
		{
			this.ServiceType = serviceType;
			this.Key = key;
			this.ImplementationType = implementationType ?? serviceType;
		}

		public Type ServiceType { get; private set; }
		public String Key { get; private set; }
		public Type ImplementationType { get; private set; }
	}
}
