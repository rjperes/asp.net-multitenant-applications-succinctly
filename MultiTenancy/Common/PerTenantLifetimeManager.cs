﻿using System;
using System.Collections.Concurrent;
using Microsoft.Practices.Unity;

namespace Common
{
	public sealed class PerTenantLifetimeManager : LifetimeManager
	{
		private readonly Guid id = Guid.NewGuid();

		private static readonly ConcurrentDictionary<String, ConcurrentDictionary<Guid, Object>> items = new ConcurrentDictionary<String, ConcurrentDictionary<Guid, Object>>();

		public override Object GetValue()
		{
			var tenant = TenantsConfiguration.GetCurrentTenant();
			var registrations = null as ConcurrentDictionary<Guid, Object>;
			Object value = null;

			if (items.TryGetValue(tenant.Name(), out registrations) == true)
			{
				registrations.TryGetValue(this.id, out value);
			}

			return (value);
		}

		public override void RemoveValue()
		{
			var tenant = TenantsConfiguration.GetCurrentTenant();
			var registrations = null as ConcurrentDictionary<Guid, Object>;

			if (items.TryGetValue(tenant.Name(), out registrations) == true)
			{
				Object value;
				registrations.TryRemove(this.id, out value);
			}
		}

		public override void SetValue(Object newValue)
		{
			var tenant = TenantsConfiguration.GetCurrentTenant();
			var registrations = items.GetOrAdd(tenant.Name(), new ConcurrentDictionary<Guid, Object>());
			registrations[this.id] = newValue;
		}
	}
}
