﻿using System;
using System.Web.Management;

namespace Common
{
	public interface IDiagnostics
	{
		Int64 IncrementCounterInstance(String instanceName, Int32 value = 1);

		void RaiseEtwEvent(String eventName, Int32 eventId);

		Guid RaiseWebEvent(Int32 eventCode, String message, Object data, Int32 eventDetailCode = 0);

		Guid RaiseWebEvent(WebBaseEvent @event);

		void Trace(Object value, String category);
	}
}
