﻿using System;
using System.Diagnostics;

namespace Common
{
	public interface ILog : ITenantAwareService
	{
		void Write(Object message, Int32 priority, TraceEventType severity, Int32 eventId = 0);

		void Write(Exception exception, Int32 eventId = 0);
	}
}
