﻿using System;
using System.Collections.Generic;
using System.Runtime.Caching;

namespace Common
{
	public interface ICache
	{
		void Remove(String key, String regionName = null);
		Object Get(String key, String regionName = null);
		void Add(String key, Object value, DateTime absoluteExpiration, String regionName = null);
		void Add(String key, Object value, TimeSpan slidingExpiration, String regionName = null);
	}
}
