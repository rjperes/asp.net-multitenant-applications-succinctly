﻿using System;
using System.Linq;
using System.Linq.Expressions;

namespace Common
{
	public interface IRepository : IDisposable
	{
		T Find<T>(params Object[] ids) where T : class;

		IQueryable<T> Query<T>(params Expression<Func<T, Object>>[] expansions) where T : class;

		void Delete<T>(T item) where T : class;

		void Save<T>(T item) where T : class;

		void Update<T>(T item) where T : class;

		void Refresh<T>(T item) where T : class;

		void Detach<T>(T item) where T : class;

		void SaveChanges();
	}
}
