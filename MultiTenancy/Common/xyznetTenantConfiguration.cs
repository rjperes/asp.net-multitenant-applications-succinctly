﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;

namespace Common
{
	[PartCreationPolicy(CreationPolicy.Shared)]
	[Export("xyz.net", typeof(ITenantConfiguration))]
	[TenantName("xyz.net")]
	[TenantTheme("xyz.net")]
	[TenantMasterPage("XyzNet")]
	[TenantCounter("C", "", InstanceName = "A Counter")]
	[TenantCounter("D", "", InstanceName = "Another Counter")]
	[TenantCounter("E", "", InstanceName = "Yet Another Counter")]
	public sealed class XyzNetTenantConfiguration : ITenantConfiguration
	{
		#region ITenantConfiguration Members

		public void OnInitialize()
		{
		}

		public void OnException(Exception ex)
		{
		}


		#endregion
	}
}
