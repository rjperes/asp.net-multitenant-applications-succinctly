﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Caching;

namespace Common
{
	public sealed class AspNetMultiTenantCache : ICache, ITenantAwareService
	{
		public static readonly ICache Instance = new AspNetMultiTenantCache();

		private AspNetMultiTenantCache()
		{
			//private constructor since this is meant to be used as a singleton
		}

		private String GetTenantKey(String key, String regionName)
		{
			var tenant = TenantsConfiguration.GetCurrentTenant().Name();
			key = String.Concat(tenant, ":", regionName, ":", key);
			return (key);
		}

		#region ICache Members
		public void Remove(String key, String regionName = null)
		{
			System.Web.HttpContext.Current.Cache.Remove(this.GetTenantKey(key, regionName));
		}

		public Object Get(String key, String regionName = null)
		{
			return (System.Web.HttpContext.Current.Cache.Get(this.GetTenantKey(key, regionName)));
		}

		public void Add(String key, Object value, DateTime absoluteExpiration, String regionName = null)
		{
			System.Web.HttpContext.Current.Cache.Add(this.GetTenantKey(key, regionName), value, null, absoluteExpiration, System.Web.Caching.Cache.NoSlidingExpiration, System.Web.Caching.CacheItemPriority.Default, null);
		}

		public void Add(String key, Object value, TimeSpan slidingExpiration, String regionName = null)
		{
			System.Web.HttpContext.Current.Cache.Add(this.GetTenantKey(key, regionName), value, null, System.Web.Caching.Cache.NoAbsoluteExpiration, slidingExpiration, System.Web.Caching.CacheItemPriority.Default, null);
		}

		#endregion
	}
}
