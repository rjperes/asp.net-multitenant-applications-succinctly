﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;

namespace Common
{
	[Serializable]
	public sealed class ServicesElementCollection : ConfigurationElementCollection, IEnumerable<ServiceElement>
	{
		protected override String ElementName
		{
			get
			{
				return (String.Empty);
			}
		}

		protected override ConfigurationElement CreateNewElement()
		{
			return (new ServiceElement());
		}

		protected override Object GetElementKey(ConfigurationElement element)
		{
			var elm = element as ServiceElement;
			return (String.Concat(elm.ServiceType, ":", elm.Key));
		}

		#region IEnumerable<TenantElement> Members

		IEnumerator<ServiceElement> IEnumerable<ServiceElement>.GetEnumerator()
		{
			foreach (var elm in this.OfType<ServiceElement>())
			{
				yield return (elm);
			}
		}

		#endregion
	}
}
