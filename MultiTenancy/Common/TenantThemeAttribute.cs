﻿using System;

namespace Common
{
	[Serializable]
	[AttributeUsage(AttributeTargets.Class, AllowMultiple = false, Inherited = true)]
	public sealed class TenantThemeAttribute : TenantAttribute
	{
		public TenantThemeAttribute(String theme)
		{
			this.Theme = theme;
		}

		public String Theme { get; private set; }
	}
}
