﻿using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Web;

namespace Common
{
	public sealed class MultiTenantTraceListener : TextWriterTraceListener
	{
		public MultiTenantTraceListener(String tenant): base(Path.Combine(HttpRuntime.BinDirectory, String.Concat(tenant, ".log")))
		{
			this.Name = tenant;
		}

		public override void WriteLine(String message)
		{
			var tenant = TenantsConfiguration.GetCurrentTenant();
			var parts = message.Split(':');
			var category = parts.First();

			if ((tenant.Name() == this.Name) && (this.Name == category))
			{
				base.WriteLine(message);
			}
		}
	}
}
