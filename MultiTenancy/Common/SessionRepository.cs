﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using NHibernate;
using NHibernate.Linq;

namespace Common
{
	public sealed class SessionRepository : IRepository
	{
		private readonly ISessionFactory sessionFactory;
		private ISession session;

		public SessionRepository(ISessionFactory sessionFactory)
		{
			this.sessionFactory = sessionFactory;
			this.session = sessionFactory.OpenSession();
			this.session.BeginTransaction();
		}

		#region IRepository Members

		public T Find<T>(params Object[] ids) where T : class
		{
			return (this.session.Get<T>(ids.Single()));
		}

		public IQueryable<T> Query<T>(params Expression<Func<T, Object>>[] expansions) where T : class
		{
			var all = this.session.Query<T>() as IQueryable<T>;

			foreach (var expansion in expansions)
			{
				all = all.Include(expansion);
			}

			return (all);
		}

		public void Delete<T>(T item) where T : class
		{
			this.session.Delete(item);
		}

		public void Save<T>(T item) where T : class
		{
			this.session.Save(item);
		}

		public void Update<T>(T item) where T : class
		{
			this.session.Update(item);
		}

		public void Refresh<T>(T item) where T : class
		{
			this.session.Refresh(item);
		}

		public void Detach<T>(T item) where T : class
		{
			this.session.Evict(item);
		}

		public void SaveChanges()
		{
			this.session.Flush();
			try
			{
				this.session.Transaction.Commit();
			}
			catch
			{
				this.session.Transaction.Rollback();
			}
			this.session.BeginTransaction();
		}

		#endregion
		
		#region IDisposable Members

		public void Dispose()
		{
			this.session.Dispose();
			this.session = null;
		}

		#endregion
	}
}
