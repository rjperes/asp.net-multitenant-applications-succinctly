﻿using System;

namespace Common
{
	public interface IConfiguration
	{
		Object GetValue(String key);
		void SetValue(String key, Object value);
		Object RemoveValue(String key);
	}
}
