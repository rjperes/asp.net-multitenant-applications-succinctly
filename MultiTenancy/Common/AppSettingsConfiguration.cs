﻿using System;
using System.Configuration;

namespace Common
{
	public class AppSettingsConfiguration : IConfiguration, ITenantAwareService
	{
		public static readonly IConfiguration Instance = new AppSettingsConfiguration();

		private AppSettingsConfiguration()
		{
		}

		private void Persist(Configuration configuration)
		{
			configuration.Save();
		}

		private Configuration Configuration
		{
			get
			{
				var configMap = new ExeConfigurationFileMap();
				configMap.ExeConfigFilename = AppDomain.CurrentDomain.BaseDirectory + this.GetCurrentTenant() + ".config";

				var configuration = ConfigurationManager.OpenMappedExeConfiguration(configMap, ConfigurationUserLevel.None);
				//var configuration = ConfigurationManager.OpenExeConfiguration(AppDomain.CurrentDomain.SetupInformation.ConfigurationFile);

				return configuration;
			}
		}

		private String GetCurrentTenant()
		{
			return TenantsConfiguration.GetCurrentTenant().Name();
		}

		#region IConfiguration Members

		public Object GetValue(String key)
		{
			var entry = this.Configuration.AppSettings.Settings[key];
			return (entry != null) ? entry.Value : null;
		}

		public void SetValue(String key, Object value)
		{
			if (value == null)
			{
				this.RemoveValue(key);
			}
			else
			{
				var configuration = this.Configuration;
				configuration.AppSettings.Settings.Add(key, value.ToString());
				this.Persist(configuration);
			}
		}

		public Object RemoveValue(String key)
		{
			var configuration = this.Configuration;
			var entry = configuration.AppSettings.Settings[key];
			
			if (entry != null)
			{
				configuration.AppSettings.Settings.Remove(key);
				this.Persist(configuration);

				return entry.Value;
			}
	
			return null;
		}

		#endregion
	}
}
