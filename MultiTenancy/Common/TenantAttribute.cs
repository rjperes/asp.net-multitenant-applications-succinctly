﻿using System;

namespace Common
{
	[Serializable]
	[AttributeUsage(AttributeTargets.Class, Inherited = true)]
	public abstract class TenantAttribute : Attribute
	{
	}
}
