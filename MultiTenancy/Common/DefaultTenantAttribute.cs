﻿using System;
using System.ComponentModel.Composition;

namespace Common
{
	[Serializable]
	[AttributeUsage(AttributeTargets.Class, AllowMultiple = false, Inherited = false)]
	[MetadataAttribute]
	public sealed class DefaultTenantAttribute : Attribute
	{
	}
}
