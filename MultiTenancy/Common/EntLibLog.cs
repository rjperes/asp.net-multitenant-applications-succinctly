﻿using System;
using System.Diagnostics;
using Microsoft.Practices.EnterpriseLibrary.Logging;

namespace Common
{
	public sealed class EntLibLog : ILog, ITenantAwareService
	{
		public static readonly ILog Instance = new EntLibLog();

		private EntLibLog()
		{
		}

		#region ILog Members

		public void Write(Exception exception, Int32 eventId = 0)
		{
			this.Write(String.Concat(exception.GetType(), ": ", exception.Message), Int32.MaxValue, TraceEventType.Error, eventId);
		}

		public void Write(Object message, Int32 priority, TraceEventType severity, Int32 eventId = 0)
		{
			try
			{
				var tenant = TenantsConfiguration.GetCurrentTenant();
				Logger.Write(message, tenant.Name(), priority, eventId, severity);
			}
			catch
			{
			}
		}

		#endregion
	}
}
