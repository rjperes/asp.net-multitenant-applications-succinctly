﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Common
{
	public sealed class XmlTenantLocationStrategy : ITenantLocationStrategy
	{
		public static readonly XmlTenantLocationStrategy Instance = new XmlTenantLocationStrategy();

		#region ITenantLocationStrategy Members

		public IDictionary<String, Type> GetTenants()
		{
			var tenants = TenantsSection.Section.Tenants.ToDictionary(x => x.Name, x => x.Type);

			foreach (var tenant in TenantsSection.Section.Tenants.OfType<TenantElement>())
			{
				if ((TenantsSection.Section.Default == tenant.Name) && (String.IsNullOrWhiteSpace(TenantsConfiguration.DefaultTenant) == true))
				{
					TenantsConfiguration.DefaultTenant = tenant.Name;
				}
			}

			return (tenants);
		}

		#endregion
	}
}
