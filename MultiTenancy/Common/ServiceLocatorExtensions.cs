﻿using System;
using Microsoft.Practices.ServiceLocation;

namespace Common
{
	public static class ServiceLocatorExtensions
	{
		public static T TryResolve<T>(this IServiceLocator locator, String name = null)
		{
			var component = default(T);

			try
			{
				component = locator.GetInstance<T>(name);
			}
			catch
			{
			}

			return (component);
		}
	}
}
