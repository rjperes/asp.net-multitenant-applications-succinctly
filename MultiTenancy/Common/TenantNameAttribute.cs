﻿using System;

namespace Common
{
	[Serializable]
	[AttributeUsage(AttributeTargets.Class, AllowMultiple = false, Inherited = true)]
	public sealed class TenantNameAttribute : TenantAttribute
	{
		public TenantNameAttribute(String name)
		{
			this.Name = name;
		}

		public String Name { get; private set; }
	}
}
