﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition.Primitives;
using System.ComponentModel.Composition.ReflectionModel;
using System.Linq;

namespace Common
{
	public static class CatalogExtensions
	{
		public static IDictionary<String, Type> GetExportedTypes<T>(this ComposablePartCatalog catalog)
		{
			return (catalog.Parts.Where(part => IsComposablePart<T>(part) == true).ToDictionary(part => part.ExportDefinitions.First().ContractName, part => ReflectionModelServices.GetPartType(part).Value));
		}

		private static Boolean IsComposablePart<T>(ComposablePartDefinition part)
		{
			return (part.ExportDefinitions.Any(def => (def.Metadata.ContainsKey("ExportTypeIdentity") == true) && (def.Metadata["ExportTypeIdentity"].Equals(typeof (T).FullName) == true)));
		}
	}
}
