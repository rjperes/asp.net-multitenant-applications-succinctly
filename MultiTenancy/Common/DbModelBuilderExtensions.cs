﻿using System;
using System.Data.Entity;

namespace Common
{
	public static class DbModelBuilderExtensions
	{
		public const String DefaultTenantColumnName = "Tenant";

		public static DbModelBuilder MakeMultiTenant<T>(this DbModelBuilder modelBuilder, String columnName = DefaultTenantColumnName) where T : class
		{
			var tenant = TenantsConfiguration.GetCurrentTenant().Name();

			modelBuilder.Entity<T>().Map(m => m.Requires(columnName).HasValue(tenant));

			return (modelBuilder);
		}
	}
}
