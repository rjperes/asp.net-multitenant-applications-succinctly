﻿using System;
using System.Diagnostics;

namespace Common
{
	[Serializable]
	[AttributeUsage(AttributeTargets.Class, AllowMultiple = true, Inherited = true)]
	public sealed class TenantCounterAttribute : TenantAttribute
	{
		public TenantCounterAttribute(String categoryName, String categoryHelp, PerformanceCounterCategoryType categoryType, String counterName, PerformanceCounterType performanceCounterType)
		{
			this.CounterName = counterName;
			this.CategoryName = categoryName;
			this.CategoryHelp = categoryHelp;
			this.CategoryType = categoryType;
			this.PerformanceCounterType = performanceCounterType;
		}

		public TenantCounterAttribute(String categoryName, String counterName) : this(categoryName, categoryName, PerformanceCounterCategoryType.MultiInstance, counterName, PerformanceCounterType.NumberOfItems32)
		{
		}

		public String InstanceName { get; set; }

		public String CounterName { get; set; }
		public PerformanceCounterType PerformanceCounterType { get; set; }
		public String CategoryName { get; private set; }
		public String CategoryHelp { get; set; }
		public PerformanceCounterCategoryType CategoryType { get; set; }
	}
}
