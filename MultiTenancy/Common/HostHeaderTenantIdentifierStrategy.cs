﻿using System;
using System.Web.Routing;

namespace Common
{
	public sealed class HostHeaderTenantIdentifierStrategy : ITenantIdentifierStrategy
	{
		#region ITenantIdentifier Members

		public String GetCurrentTenant(RequestContext context)
		{
			return (context.HttpContext.Request.Url.Host.ToLower());
		}

		#endregion
	}
}
