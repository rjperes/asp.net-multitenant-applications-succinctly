﻿using System;
using System.Diagnostics.Tracing;
using System.Linq;
using Microsoft.Diagnostics.Tracing;

namespace Common
{
	[Microsoft.Diagnostics.Tracing.EventData(Name = "MultitenantEventData")]
	public class MultitenantEventData
	{
		public String TenantId { get; set; }
		public Int32 EventId { get; set; }
	}

	[Microsoft.Diagnostics.Tracing.EventSource(Name = MultiTenantEventSource.SourceName, Guid = MultiTenantEventSource.SourceId)]
	public sealed class MultiTenantEventSource : Microsoft.Diagnostics.Tracing.EventSource
	{
		public const String SourceName = "MultitenantEventSource";
		public const String SourceId = "AAAAAAAA-BBBB-CCCC-DDDD-EEEEEEEEEEEE";
		public static readonly MultiTenantEventSource Instance = new MultiTenantEventSource();

		private MultiTenantEventSource()
		{
		}

		public void Raise(String eventName, Int32 eventId)
		{
			var tenant = TenantsConfiguration.GetCurrentTenant();

			var data = new MultitenantEventData();
			data.EventId = eventId;
			data.TenantId = tenant.Name();

			var options = new EventSourceOptions();

			//this.WriteEvent(eventId, new Object[] { tenant.Name() }.Concat(args).ToArray());


			this.Write<MultitenantEventData>(eventName, options, data);
		}
	}
}
