﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Web.Routing;

namespace Common
{
	public sealed class SourceIPTenantIdentifierStrategy : ITenantIdentifierStrategy
	{
		private readonly Dictionary<Tuple<IPAddress, IPAddress>, String> networks = new Dictionary<Tuple<IPAddress, IPAddress>, String>();

		public SourceIPTenantIdentifierStrategy Add(IPAddress ipAddress, Int32 netmaskBits, String name)
		{
			return (this.Add(ipAddress, SubnetMask.CreateByNetBitLength(netmaskBits), name));
		}

		public SourceIPTenantIdentifierStrategy Add(IPAddress ipAddress, IPAddress netmaskAddress, String name)
		{
			this.networks[new Tuple<IPAddress, IPAddress>(ipAddress, netmaskAddress)] = name.ToLower();
			return (this);
		}

		public SourceIPTenantIdentifierStrategy Add(IPAddress ipAddress, String name)
		{
			return (this.Add(ipAddress, null, name));
		}

		#region ITenantIdentifierStrategy Members

		public String GetCurrentTenant(RequestContext context)
		{
			var ip = IPAddress.Parse(context.HttpContext.Request.UserHostAddress);

			foreach (var entry in this.networks)
			{
				if (entry.Key.Item2 == null)
				{
					if (ip.Equals(entry.Key.Item1))
					{
						return (entry.Value.ToLower());
					}
				}
				else
				{
					if (ip.IsInSameSubnet(entry.Key.Item1, entry.Key.Item2) == true)
					{
						return (entry.Value);
					}
				}
			}

			return (null);
		}

		#endregion
	}
}
