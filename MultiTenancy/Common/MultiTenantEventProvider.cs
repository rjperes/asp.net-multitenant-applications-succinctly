﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web.Configuration;
using System.Web.Management;

namespace Common
{
	public sealed class MultiTenantEventProvider : WebEventProvider
	{
		private static readonly IDictionary<String, MultiTenantEventProvider> providers = new ConcurrentDictionary<String, MultiTenantEventProvider>();

		private const String TenantKey = "tenant";

		public String Tenant { get; private set; }

		public event EventHandler<MultiTenantEventArgs> Event;

		public static void RegisterEvent(EventHandler<MultiTenantEventArgs> handler)
		{
			var tenant = TenantsConfiguration.GetCurrentTenant();
			var provider = FindProvider(tenant.Name());

			if (provider != null)
			{
				provider.Event += handler;
			}
		}

		public static MultiTenantEventProvider FindProvider(String tenantId)
		{
			var provider = null as MultiTenantEventProvider;

			providers.TryGetValue(tenantId, out provider);

			return provider;
		}

		public override void Initialize(String name, NameValueCollection config)
		{
			var tenant = config.Get(TenantKey);

			if (String.IsNullOrWhiteSpace(tenant) == true)
			{
				throw (new InvalidOperationException("Missing tenant name."));
			}

			config.Remove(TenantKey);

			this.Tenant = tenant;

			providers[tenant] = this;

			base.Initialize(name, config);
		}

		public override void Flush()
		{
		}

		public override void ProcessEvent(WebBaseEvent raisedEvent)
		{
			var evt = raisedEvent as MultiTenantWebEvent;

			if (evt != null)
			{
				var tenant = TenantsConfiguration.GetCurrentTenant();

				if (tenant.Name() == evt.Tenant)
				{
					var handler = this.Event;

					if (handler != null)
					{
						handler(this, new MultiTenantEventArgs(this, evt));
					}
				}
			}
		}

		public override void Shutdown()
		{
		}
	}
}
