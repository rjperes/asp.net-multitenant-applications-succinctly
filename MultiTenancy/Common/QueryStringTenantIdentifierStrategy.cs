﻿using System;
using System.Web.Routing;

namespace Common
{
	public sealed class QueryStringTenantIdentifierStrategy : ITenantIdentifierStrategy
	{
		public const String DefaultTenantKey = "Tenant";

		private readonly String tenantKey;

		public QueryStringTenantIdentifierStrategy() : this(DefaultTenantKey)
		{
		}

		public QueryStringTenantIdentifierStrategy(String tenantKey)
		{
			this.tenantKey = tenantKey;
		}

		#region ITenantIdentifier Members

		public String GetCurrentTenant(RequestContext context)
		{
			return((context.HttpContext.Request.QueryString[this.tenantKey] ?? String.Empty).ToLower());
		}

		#endregion
	}
}
