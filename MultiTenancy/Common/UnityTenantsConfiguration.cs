﻿using Microsoft.Practices.ServiceLocation;
using Microsoft.Practices.Unity;
using Model;

namespace Common
{
	public static class UnityTenantsConfiguration
	{
		public static void RegisterTypes(IUnityContainer container)
		{
			ServiceLocator.SetLocatorProvider(() => new UnityServiceLocator(container));

			container.RegisterType<IRepository>(new PerRequestLifetimeManager(), new InjectionFactory(x =>
					new ContextRepository(typeof(MultiTenantContext), TenantsConfiguration.GetCurrentTenant().Name())));

			// the one and only tenant identifier
			container.RegisterInstance<ITenantIdentifierStrategy>(TenantsConfiguration.Identifiers.HostHeader);
			//container.RegisterInstance<ITenantIdentifierStrategy>(TenantsConfiguration.Identifiers.QueryString);
			container.RegisterInstance<ITenantLocationStrategy>(TenantsConfiguration.Locations.Mef(typeof(AbcComTenantConfiguration).Assembly));
			//container.RegisterInstance<ITenantLocationStrategy>(TenantsConfiguration.Locations.Dictionary(new Dictionary<String, Type> { { "abc.com", typeof(abccomTenantConfiguration) }, { "xyz.net", typeof(xyznetTenantConfiguration) } }));

			//container.RegisterInstance<IConfiguration>(AppSettingsConfiguration.Instance);
			container.RegisterInstance<IConfiguration>(AppSettingsConfiguration.Instance);
			container.RegisterInstance<ICache>(AspNetMultiTenantCache.Instance);
			container.RegisterInstance<IDiagnostics>(MultiTenantDiagnostics.Instance);
			container.RegisterInstance<ILog>(EntLibLog.Instance);

			TenantsConfiguration.DefaultTenant = "abc.com";

			foreach (var tenant in container.Resolve<ITenantLocationStrategy>().GetTenants())
			{
				container.RegisterType(typeof(ITenantConfiguration), tenant.Value, tenant.Key, new ContainerControlledLifetimeManager());
			}
		}
	}
}
