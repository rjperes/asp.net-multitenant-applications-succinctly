﻿using System;
using System.Collections.Generic;

namespace Common
{
	public interface ITenantConfiguration
	{
		void OnInitialize();
		void OnException(Exception ex);
	}
}
