﻿using System;
using System.Web.Management;

namespace Common
{
	public sealed class MultiTenantWebEvent : WebBaseEvent
	{
		public MultiTenantWebEvent(String message, Object eventSource, Object data, Int32 eventCode) : this(message, eventSource, eventCode, data, 0)
		{
		}

		public MultiTenantWebEvent(String message, Object eventSource, Int32 eventCode, Object data, Int32 eventDetailCode) : base(message, eventSource, eventCode, eventDetailCode)
		{
			var tenant = TenantsConfiguration.GetCurrentTenant();
			this.Tenant = tenant.Name();
			this.Data = data;
		}

		public Object Data { get; private set; }

		public String Tenant { get; private set; }
	}
}
