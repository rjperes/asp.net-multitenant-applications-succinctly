﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Diagnostics;

namespace Common
{
	[PartCreationPolicy(CreationPolicy.Shared)]
	[Export("abc.com", typeof(ITenantConfiguration))]
	[ExportMetadata("Default", true)]
	[TenantName("abc.com")]
	[TenantTheme("abc.com")]
	[TenantMasterPage("AbcCom")]
	[TenantCounter("A Counter", "", CategoryType = PerformanceCounterCategoryType.MultiInstance, InstanceName = "A Counter")]
	[TenantCounter("B", "", CategoryType = PerformanceCounterCategoryType.MultiInstance, InstanceName = "Another Counter")]
	[TenantCounter("C", "", CategoryType = PerformanceCounterCategoryType.MultiInstance, InstanceName = "Yet Another Counter")]
	public sealed class AbcComTenantConfiguration : ITenantConfiguration
	{
		#region ITenantConfiguration Members

		public void OnInitialize()
		{
		}

		public void OnException(Exception ex)
		{
		}

		#endregion
	}
}
