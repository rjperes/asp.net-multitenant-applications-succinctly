﻿using System;
using System.Reflection;
using System.Threading;

namespace Common
{
	public static class ThreadExtensions
	{
		private static readonly FieldInfo NameField = typeof(Thread).GetField("m_Name", BindingFlags.Instance | BindingFlags.NonPublic);

		public static void Rename(this Thread thread, String name)
		{
			lock (thread)
			{
				NameField.SetValue(thread, name);
			}
		}
	}
}
