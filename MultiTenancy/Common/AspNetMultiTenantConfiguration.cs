﻿using System;
using System.Web;

namespace Common
{
	public sealed class AspNetMultiTenantConfiguration : IConfiguration, ITenantAwareService
	{
		public static readonly IConfiguration Instance = new AspNetMultiTenantConfiguration();

		private String GetTenantKey(String key)
		{
			var tenant = TenantsConfiguration.GetCurrentTenant();
			return (String.Concat(tenant.Name(), ":", key));
		}

		#region IConfiguration Members

		public Object GetValue(String key)
		{
			try
			{
				HttpContext.Current.Application.Lock();
				return (HttpContext.Current.Application[this.GetTenantKey(key)]);
			}
			finally
			{
				HttpContext.Current.Application.UnLock();
			}
		}

		public void SetValue(String key, Object value)
		{
			if (value == null)
			{
				this.RemoveValue(key);
				return;
			}

			try
			{
				HttpContext.Current.Application.Lock();
				HttpContext.Current.Application[this.GetTenantKey(key)] = value;
			}
			finally
			{
				HttpContext.Current.Application.UnLock();
			}
		}

		public Object RemoveValue(String key)
		{
			try
			{
				HttpContext.Current.Application.Lock();
				key = this.GetTenantKey(key);
				var value = HttpContext.Current.Application[key];
				if (value != null)
				{
					HttpContext.Current.Application.Remove(key);
				}
				return (value);
			}
			finally
			{
				HttpContext.Current.Application.UnLock();
			}
		}

		#endregion
	}
}
