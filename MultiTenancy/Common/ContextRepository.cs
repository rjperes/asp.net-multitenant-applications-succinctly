﻿using System;
using System.Data.Entity;
using System.Diagnostics;
using System.Linq;
using System.Linq.Expressions;

namespace Common
{
	public sealed class ContextRepository : IRepository
	{
		private readonly Boolean contextOwned;
		private DbContext context;

		public ContextRepository(Type contextType, String nameOrConnectionString = null) : this(Activator.CreateInstance(contextType, nameOrConnectionString as Object ?? new Object[0]) as DbContext)
		{
			this.contextOwned = true;
		}

		public ContextRepository(DbContext context)
		{
			this.context = context;
			this.context.Database.Log = x => Debug.WriteLine(x);
			this.contextOwned = false;
		}


		#region IRepository Members

		public T Find<T>(params Object[] ids) where T : class
		{
			return (this.context.Set<T>().Find(ids));
		}

		public IQueryable<T> Query<T>(params Expression<Func<T, Object>>[] expansions) where T : class
		{
			var all = this.context.Set<T>() as IQueryable<T>;

			foreach (var expansion in expansions)
			{
				all = all.Include(expansion);
			}

			return (all);
		}

		public void Delete<T>(T item) where T : class
		{
			this.context.Set<T>().Remove(item);
		}

		public void Save<T>(T item) where T : class
		{
			this.context.Set<T>().Add(item);
		}

		public void Update<T>(T item) where T : class
		{
			this.context.Entry(item).State = EntityState.Modified;
		}

		public void Refresh<T>(T item) where T : class
		{
			this.context.Entry(item).Reload();
		}

		public void Detach<T>(T item) where T : class
		{
			this.context.Entry(item).State = EntityState.Detached;
		}

		public void SaveChanges()
		{
			this.context.SaveChanges();
		}

		#endregion

		#region IDisposable Members

		public void Dispose()
		{
			if (this.contextOwned == true)
			{
				if (this.context != null)
				{
					this.context.Dispose();
					this.context = null;
				}
			}
		}

		#endregion
	}
}
