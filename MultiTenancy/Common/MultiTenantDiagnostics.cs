﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Web.Management;
using Microsoft.Diagnostics.Tracing;

namespace Common
{
	public sealed class MultiTenantDiagnostics : IDiagnostics, ITenantAwareService
	{
		public static readonly IDiagnostics Instance = new MultiTenantDiagnostics();

		private MultiTenantDiagnostics()
		{
		}

		public Int64 IncrementCounterInstance(String instanceName, Int32 value = 1)
		{
			var tenant = TenantsConfiguration.GetCurrentTenant();

			using (var pc = new PerformanceCounter("Tenants", tenant.Name(), String.Concat(tenant.Name(), ":", instanceName), false))
			{
				pc.RawValue += value;
				return (pc.RawValue);
			}
		}

		public void RaiseEtwEvent(String eventName, Int32 eventId)
		{
			MultiTenantEventSource.Instance.Raise(eventName, eventId);
		}

		public Guid RaiseWebEvent(WebBaseEvent @event)
		{
			return (this.RaiseWebEvent(@event.EventCode, @event.Message, @event.EventDetailCode));
		}

		public Guid RaiseWebEvent(Int32 eventCode, String message, Object data, Int32 eventDetailCode = 0)
		{
			var tenant = TenantsConfiguration.GetCurrentTenant();
			var evt = new MultiTenantWebEvent(message, tenant, eventCode, data, eventDetailCode);
			evt.Raise();
			return (evt.EventID);
		}

		public void Trace(Object value, String category)
		{
			var tenant = TenantsConfiguration.GetCurrentTenant();
			System.Diagnostics.Trace.AutoFlush = true;
			System.Diagnostics.Trace.WriteLine(String.Concat(tenant.Name(), ": ", value), category);
		}
	}
}
