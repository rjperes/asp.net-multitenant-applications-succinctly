﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;

namespace Common
{
	[Serializable]
	public sealed class TenantsElementCollection : ConfigurationElementCollection, IEnumerable<TenantElement>
	{
		protected override String ElementName
		{
			get
			{
				return (String.Empty);
			}
		}

		protected override ConfigurationElement CreateNewElement()
		{
			return (new TenantElement());
		}

		protected override Object GetElementKey(ConfigurationElement element)
		{
			var elm = element as TenantElement;
			return (String.Concat(elm.Name, ":", elm.Type));
		}

		#region IEnumerable<TenantElement> Members

		IEnumerator<TenantElement> IEnumerable<TenantElement>.GetEnumerator()
		{
			foreach (var elm in this.OfType<TenantElement>())
			{
				yield return (elm);
			}
		}

		#endregion
	}
}
