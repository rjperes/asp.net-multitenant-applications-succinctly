﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Common
{
	public sealed class DictionaryTenantLocationStrategy : ITenantLocationStrategy
	{
		private readonly IDictionary<String, Type> tenants;

		public DictionaryTenantLocationStrategy(params ITenantConfiguration[] tenants) : this(tenants.AsEnumerable())
		{
		}

		public DictionaryTenantLocationStrategy(IEnumerable<ITenantConfiguration> tenants) : this(tenants.ToDictionary(x => x.Name(), x => x.GetType()))
		{
		}

		public DictionaryTenantLocationStrategy(IDictionary<String, Type> tenants)
		{
			this.tenants = tenants;
		}

		public DictionaryTenantLocationStrategy Add<T>() where T : ITenantConfiguration, new()
		{
			return (this.Add(new ITenantConfiguration[] { new T() }));
		}

		public DictionaryTenantLocationStrategy Add(params ITenantConfiguration [] tenants)
		{
			return (this.Add(tenants.AsEnumerable()));
		}

		public DictionaryTenantLocationStrategy Add(Func<ITenantConfiguration> tenantFunc)
		{
			return (this.Add(tenantFunc()));
		}

		public DictionaryTenantLocationStrategy Add(IEnumerable<ITenantConfiguration> tenants)
		{
			foreach (var tenant in tenants)
			{
				this.Add(tenant.Name(), tenant.GetType());
			}

			return (this);
		}

		public DictionaryTenantLocationStrategy Add(String name, Type type)
		{
			this.tenants[name] = type;
			return (this);
		}

		#region ITenantLocationStrategy Members

		public IDictionary<String, Type> GetTenants()
		{
			return (this.tenants);
		}

		#endregion
	}
}
