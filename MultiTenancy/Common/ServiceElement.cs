﻿using System;
using System.ComponentModel;
using System.Configuration;

namespace Common
{
	[Serializable]
	public sealed class ServiceElement : ConfigurationElement
	{
		[ConfigurationProperty("serviceType", IsKey = true, IsRequired = true)]
		[TypeConverter(typeof(TypeTypeConverter))]
		public Type ServiceType
		{
			get
			{
				return (this["serviceType"] as Type);
			}
			set
			{
				this["serviceType"] = value;
			}
		}

		[ConfigurationProperty("implementationType")]
		[TypeConverter(typeof(TypeTypeConverter))]
		public Type ImplementationType
		{
			get
			{
				return (this["implementationType"] as Type) ?? this.ServiceType;
			}
			set
			{
				this["implementationType"] = value;
			}
		}

		[ConfigurationProperty("key", IsKey = true, DefaultValue = "")]
		public String Key
		{
			get
			{
				return (this["key"] as String);
			}
			set
			{
				this["key"] = value;
			}
		}
	}
}