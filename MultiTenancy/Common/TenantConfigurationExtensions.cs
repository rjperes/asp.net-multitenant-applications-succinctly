﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace Common
{
	public static class TenantConfigurationExtensions
	{
		public static String Name(this ITenantConfiguration tenant)
		{
			var attr = Attribute.GetCustomAttribute(tenant.GetType(), typeof(TenantNameAttribute), true) as TenantNameAttribute;

			return attr.Name;
		}

		public static String Theme(this ITenantConfiguration tenant)
		{
			var attr = Attribute.GetCustomAttribute(tenant.GetType(), typeof(TenantThemeAttribute), true) as TenantThemeAttribute;

			return attr.Theme;
		}

		public static String MasterPage(this ITenantConfiguration tenant)
		{
			var attr = Attribute.GetCustomAttribute(tenant.GetType(), typeof(TenantMasterPageAttribute), true) as TenantMasterPageAttribute;

			return attr.MasterPage;
		}

		public static IReadOnlyDictionary<String, Object> Properties(this ITenantConfiguration tenant)
		{
			var attrs = Attribute.GetCustomAttributes(tenant.GetType(), typeof(TenantPropertyAttribute), true) as TenantPropertyAttribute[];

			return new ReadOnlyDictionary<String, Object>(attrs.ToDictionary(x => x.PropertyName, x => x.PropertyValue));
		}

		public static IEnumerable<TenantCounterAttribute> Counters(this ITenantConfiguration tenant)
		{
			var attrs = Attribute.GetCustomAttributes(tenant.GetType(), typeof(TenantCounterAttribute), true) as TenantCounterAttribute[];

			return attrs;
		}

		public static Object GetService(this ITenantConfiguration tenant, Type serviceType, String key = "")
		{
			var attrs = Attribute.GetCustomAttributes(tenant.GetType(), typeof(TenantServiceAttribute), true) as TenantServiceAttribute[];
			var attr = attrs.SingleOrDefault(x => x.ServiceType == serviceType && x.Key == key);

			return Activator.CreateInstance(attr.ImplementationType);
		}

		public static T GetService<T>(this ITenantConfiguration tenant, String key = "")
		{
			return (T) GetService(tenant, typeof(T), key);
		}
	}
}
