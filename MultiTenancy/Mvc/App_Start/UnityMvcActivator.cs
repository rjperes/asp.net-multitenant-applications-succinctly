using System.Linq;
using System.Web;
using System.Web.Mvc;
using AspNet.Identity.EntityFramework.Multitenant;
using Common;
using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security;
using Microsoft.Practices.Unity;
using Microsoft.Practices.Unity.Mvc;
using Microsoft.Web.Infrastructure.DynamicModuleHelper;
using Mvc.Models;

[assembly: WebActivatorEx.PreApplicationStartMethod(typeof(Mvc.UnityWebActivator), "Start")]
[assembly: WebActivatorEx.ApplicationShutdownMethod(typeof(Mvc.UnityWebActivator), "Shutdown")]

namespace Mvc
{
	/// <summary>Provides the bootstrapping for integrating Unity with ASP.NET MVC.</summary>
	public static class UnityWebActivator
	{
		/// <summary>Integrates Unity when the application starts.</summary>
		public static void Start()
		{
			var container = UnityConfig.GetConfiguredContainer();

			FilterProviders.Providers.Remove(FilterProviders.Providers.OfType<FilterAttributeFilterProvider>().First());
			FilterProviders.Providers.Add(new UnityFilterAttributeFilterProvider(container));

			DependencyResolver.SetResolver(new UnityDependencyResolver(container));

			container.RegisterType<IAuthenticationManager>(new InjectionFactory(x =>
				{
					return HttpContext.Current.GetOwinContext().Authentication;
				}));

			container.RegisterType<IUserStore<ApplicationUser>>(new PerRequestLifetimeManager(), new InjectionFactory(x =>
				{
					var tenant = TenantsConfiguration.GetCurrentTenant();
					return new MultitenantUserStore<ApplicationUser>(new ApplicationDbContext()) { TenantId = tenant.Name() };
				}));

			DynamicModuleUtility.RegisterModule(typeof(UnityPerRequestHttpModule));
		}

		/// <summary>Disposes the Unity container when the application is shut down.</summary>
		public static void Shutdown()
		{
			var container = UnityConfig.GetConfiguredContainer();
			container.Dispose();
		}
	}
}