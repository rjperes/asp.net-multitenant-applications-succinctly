﻿using System;
using System.Linq;
using System.Web.Optimization;
using Common;

namespace Mvc
{
	public class BundleConfig
	{
		public static void RegisterBundles(BundleCollection bundles)
		{
			foreach (var tenant in TenantsConfiguration.GetTenants())
			{
				var virtualPath = String.Format("~/{0}", tenant.Name());
				var physicalPath = String.Format("~/Content/{0}", tenant.Theme());

				if (BundleTable.Bundles.Any(b => b.Path == virtualPath) == false)
				{
					var bundle = new StyleBundle(virtualPath).IncludeDirectory(physicalPath, "*.css");
					BundleTable.Bundles.Add(bundle);
				}
			}
		}
	}
}