﻿using System.Web.Mvc;
using Common;

namespace Mvc
{
	public class TenantConfig
	{
		public static void Config()
		{
			ViewEngines.Engines.Clear();
			ViewEngines.Engines.Add(new MultiTenantRazorViewEngine(true));

			GlobalFilters.Filters.Add(new MultiTenantActionFilter());

			TenantsConfiguration.Initialize();
		}
	}
}