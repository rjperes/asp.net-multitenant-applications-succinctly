﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using Common;

namespace Mvc
{
	class MyRouteHandler : MvcRouteHandler
	{
		protected override IHttpHandler GetHttpHandler(RequestContext requestContext)
		{
			var tenant = TenantsConfiguration.GetCurrentTenant();
			return base.GetHttpHandler(requestContext);
		}
	}

	class MyRouteConstraint : IRouteConstraint
	{
		public Boolean Match(HttpContextBase httpContext, Route route, String parameterName, RouteValueDictionary values, RouteDirection routeDirection)
		{
			return true;
		}
	}

	public class RouteConfig
	{
		public static void RegisterRoutes(RouteCollection routes)
		{
			routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

			/*routes.MapRoute(
				name: "Default",
				url: "{controller}/{action}/{id}",
				defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
			);*/

			//var customRoute = new Route("{controller}/{action}/{id}", new RouteValueDictionary(new { controller = "Home", action = "Index", id = UrlParameter.Optional }), new RouteValueDictionary(new Dictionary<String, Object> { { "my", new MyRouteConstraint() } }), new MyRouteHandler());
			var customRoute = new Route("{controller}/{action}/{id}", new RouteValueDictionary(new { controller = "Home", action = "Index", id = UrlParameter.Optional }), new MyRouteHandler());
			routes.Add(customRoute);
		}
	}
}
