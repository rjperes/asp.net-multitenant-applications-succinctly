﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Common;
using NHibernate.Util;

namespace Mvc
{
	[Serializable]
	[AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, AllowMultiple = false, Inherited = true)]
	public sealed class AllowedTenantsAttribute : AuthorizeAttribute
	{
		public AllowedTenantsAttribute(params String [] tenants)
		{
			this.Tenants = tenants;
		}

		public IEnumerable<String> Tenants { get; private set; }

		protected override Boolean AuthorizeCore(HttpContextBase httpContext)
		{
			var tenant = TenantsConfiguration.GetCurrentTenant();

			return this.Tenants.Any(x => x == tenant.Name());
		}
	}
}