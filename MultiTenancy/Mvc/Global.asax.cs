﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Management;
using System.Net;
using System.Net.NetworkInformation;
using System.Web;
using System.Web.Instrumentation;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using Common;
using Microsoft.Practices.ServiceLocation;
using Microsoft.Practices.Unity;
using NHibernate;
using NHibernate.Cfg;

namespace Mvc
{
	
	public class MvcApplication : HttpApplication
	{
		protected void Application_BeginRequest()
		{
			var tenant = TenantsConfiguration.GetCurrentTenant().Name();
			var sessionFactory = ServiceLocator.Current.TryResolve<ISessionFactory>(tenant);

			if (sessionFactory == null)
			{
				this.SetupSessionFactory(tenant);
			}
		}

		private void SetupSessionFactory(String tenant)
		{
			return;
			var cfg = new Configuration().DataBaseIntegration(x =>
				{
					x.ConnectionStringName = tenant;
					//rest goes here
				});
			//etc
			var unity = ServiceLocator.Current.GetInstance<IUnityContainer>();
			unity.RegisterInstance<ISessionFactory>(tenant, cfg.BuildSessionFactory());
		}

		protected void Application_Start()
		{
			var section = TenantsSection.Section;

			
			//set up Unity
			//var unity = new UnityContainer();
			//UnityTenantsConfiguration.RegisterTypes(unity);
			/*using (var ctx = new LocalhostContext())
			{
				ctx.Database.Log = x => Trace.WriteLine(x);
				var stores = ctx.DataStores.ToList();
				ctx.DataStores.Add(new DataStore { Data = "abc" });
				ctx.SaveChanges();
			}*/

			//rest goes here
			BundleConfig.RegisterBundles(BundleTable.Bundles);
			AreaRegistration.RegisterAllAreas();
			RouteConfig.RegisterRoutes(RouteTable.Routes);
			TenantConfig.Config();
		}
	}
}

