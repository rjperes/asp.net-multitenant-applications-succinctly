﻿using System;
using System.Globalization;
using System.IO;
using System.Security.Principal;
using System.Text;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using NUnit.Framework;

namespace Console
{
	[SetUpFixture]
	public static class MultitenantSetup
	{
		[SetUp]
		public static void Setup()
		{
			var req = new HttpRequest(filename: String.Empty, url: "http://abc.com/", queryString: String.Empty);
			req.Headers["HTTP_HOST"] = "abc.com";

			var response = new StringBuilder();
			var res = new HttpResponse(new StringWriter(response));

			var ctx = new HttpContext(req, res);
			var principal = new GenericPrincipal(new GenericIdentity("Username"), new [] { "Role" });
			var culture = new CultureInfo("pt-PT");

			Thread.CurrentThread.CurrentCulture = culture;
			Thread.CurrentThread.CurrentUICulture = Thread.CurrentThread.CurrentCulture;

			HttpContext.Current = ctx;
			HttpContext.Current.User = principal;
		}

		public static void SetupController(this Controller controller)
		{
			var data = new RouteData();
			data.Values["action"] = "Action1";
			data.Values["controller"] = "Controller1";

			var context = new ControllerContext(new RequestContext(controller.HttpContext, data), controller);
			controller.ControllerContext = context;
		}
	}
}
