﻿using System.Web.Mvc;
using Common;

namespace Mvc.Controllers
{
	public abstract class MultiTenantController: Controller
	{
		public IDiagnostics Diagnostics
		{
			get { return (TenantsConfiguration.Services.Diagnostics); }
		}

		public ITenantConfiguration CurrentTenant
		{
			get { return (TenantsConfiguration.GetCurrentTenant()); }
		}

		public ICache Cache
		{
			get { return (TenantsConfiguration.Services.Cache); }
		}

		public ILog Log
		{
			get { return (TenantsConfiguration.Services.Log); }
		}

		public IConfiguration Configuration
		{
			get { return (TenantsConfiguration.Services.Configuration); }
		}
	}
}