﻿using System.Diagnostics;
using System.Linq;
using System.Web.Mvc;
using Microsoft.Practices.ServiceLocation;
using Model;
using IRepository = Common.IRepository;

namespace Mvc.Controllers
{
	public class HomeController : MultiTenantController
	{
		// GET: Home
		public ActionResult Index()
		{
			System.Diagnostics.Trace.WriteLine("Before presenting a view");

			this.Log.Write("TEST", 0, TraceEventType.Information);
			var val = this.Diagnostics.IncrementCounterInstance("A Counter");
			this.Diagnostics.RaiseEtwEvent("TEST", 1);
			var eventId = this.Diagnostics.RaiseWebEvent(100001, "TEST", null);
			this.Diagnostics.Trace("TEST", "TEST");
			var value = this.Configuration.GetValue("aaa");

			using (var repository = ServiceLocator.Current.GetInstance<IRepository>())
			{
				var products = repository.Query<Product>(x => x.OrderDetails).ToList();
				return (this.View());
			}
		}
	}
}