﻿using System;
using System.Security.Claims;
using System.Threading.Tasks;
using AspNet.Identity.EntityFramework.Multitenant;
using Microsoft.AspNet.Identity;

namespace Mvc.Models
{
	public class ApplicationUser : MultitenantIdentityUser
	{
		public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager)
		{
			// Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
			var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
			// Add custom user claims here
			return userIdentity;
		}

		public string HashedTwoFactorAuthCode { get; set; }
		public DateTime? DateTwoFactorAuthCodeIssued { get; set; }
	}
}