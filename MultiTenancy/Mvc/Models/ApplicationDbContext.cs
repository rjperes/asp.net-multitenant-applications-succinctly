﻿using System.Data.Entity;
using AspNet.Identity.EntityFramework.Multitenant;

namespace Mvc.Models
{
	public class ApplicationDbContext : MultitenantIdentityDbContext<ApplicationUser>
	{
		static ApplicationDbContext()
		{
			Database.SetInitializer(new DropCreateDatabaseIfModelChanges<ApplicationDbContext>());
		}

		public ApplicationDbContext() : base("DefaultConnection")
		{
		}

		public static ApplicationDbContext Create()
		{
			return new ApplicationDbContext();
		}
	}
}