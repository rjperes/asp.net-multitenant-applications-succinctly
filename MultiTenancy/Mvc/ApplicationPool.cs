using System;
using System.Globalization;
using System.IO;
using System.Management;

namespace Mvc
{
	public class ApplicationPool
	{
		/// <summary>
		/// Defines the actions that can be performed on an application pool.
		/// </summary>
		public enum ApplicationPoolAction
		{
			/// <summary>
			/// Starts the application pool.
			/// </summary>
			Start = 1,

			/// <summary>
			/// Stops the application pool.
			/// </summary>
			Stop = 2,

			/// <summary>
			/// Stops and restarts the application pool.
			/// </summary>
			Restart = 3,

			/// <summary>
			/// Recycles an application pool.
			/// </summary>
			Recycle = 4
		}

		#region Public Instance Properties

		/// <summary>
		/// The name of the server on which to perform the action. The default
		/// is the local computer.
		/// </summary>
		public string Server
		{
			get
			{
				if (_server == null)
				{
					_server = Environment.MachineName;
				}
				return _server;
			}
			set { _server = value; }
		}

		/// <summary>
		/// The name of the application pool on which to perform the action.
		/// </summary>
		public string PoolName
		{
			get { return _poolName; }
			set { _poolName = value; }
		}

		/// <summary>
		/// The action that should be performed on the application pool.
		/// </summary>
		public ApplicationPoolAction Action
		{
			get { return _action; }
			set { _action = value; }
		}

		#endregion Public Instance Properties

		#region Private Instance Properties

		private ManagementScope GetScope(out int iisversion)
		{
			ManagementScope scope = null;

			/*try
			{
				scope = new ManagementScope(@"\\" + Server + "\\root\\MicrosoftIISv2");
				scope.Connect();
				iisversion = 6;
				return scope;
			}
			catch (Exception ex)
			{
			}*/

			try
			{
				scope = new ManagementScope(@"\\" + Server + "\\root\\WebAdministration");
				scope.Connect();
				iisversion = 7;
				return scope;
			}
			catch (Exception ex)
			{
			}

			iisversion = 0;
			return scope;
		}

		#endregion Private Instance Properties

		#region Override implementation of Task

		public void ExecuteTask()
		{

			try
			{
				ManagementPath path = null;
				int iisversion = 0;
				ManagementScope scope = GetScope(out iisversion);

				switch (iisversion)
				{
					case 6:
						path = new ManagementPath("IIsApplicationPool='W3SVC/AppPools/" + PoolName + "'");
						break;
					case 7:
						path = new ManagementPath("ApplicationPool.Name='" + PoolName + "'");
						break;
					default:
						throw new Exception("Unable to identify IIS version");
				}

				ManagementObject pool = new ManagementObject(scope, path, null);

				if (Action == ApplicationPoolAction.Stop || Action == ApplicationPoolAction.Restart)
				{
					pool.InvokeMethod("Stop", new object[0]);
				}

				if (Action == ApplicationPoolAction.Start || Action == ApplicationPoolAction.Restart)
				{
					pool.InvokeMethod("Start", new object[0]);
				}

				if (Action == ApplicationPoolAction.Recycle)
				{
					pool.InvokeMethod("Recycle", new object[0]);
				}
			}
			catch (Exception ex)
			{
			}
		}

		#endregion Override implementation of Task

		#region Private Instance Methods

		private string GetActionFinish()
		{
			switch (Action)
			{
				case ApplicationPoolAction.Restart:
					return "Restarted";
				case ApplicationPoolAction.Start:
					return "Started";
				case ApplicationPoolAction.Stop:
					return "Stopped";
				default:
					return "Recycled";
			}
		}

		private string GetActionInProgress()
		{
			switch (Action)
			{
				case ApplicationPoolAction.Restart:
					return "restarting";
				case ApplicationPoolAction.Start:
					return "starting";
				case ApplicationPoolAction.Stop:
					return "stopping";
				default:
					return "recycling";
			}
		}

		#endregion Private Instance Methods

		#region Private Instance Fields

		private string _server;
		private string _poolName;
		private ApplicationPoolAction _action;

		#endregion Private Instance Fields
	}
}
