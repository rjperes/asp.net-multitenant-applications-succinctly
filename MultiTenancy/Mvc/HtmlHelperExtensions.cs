﻿using System;
using System.Web;
using System.Web.Mvc;
using Common;
using Microsoft.Practices.ServiceLocation;

namespace Mvc
{
	public static class HtmlHelperExtensions
	{
		private const String Script = "<script type=\"text/javascript\">// <![CDATA[" +
		"(function(i,s,o,g,r,a,m){{i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){{" +
		"  (i[r].q=i[r].q||[]).push(arguments)}},i[r].l=1*new Date();a=s.createElement(o)," +
		"  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)" +
		"  }})(window,document,'script','//www.google-analytics.com/analytics.js','ga');" +
		"  ga('create', '{0}', 'auto');" +
		"  ga('send', 'pageview');" +
		"// ]]></script>";

		public static IHtmlString GoogleAnalytics(this HtmlHelper html)
		{
			var config = ServiceLocator.Current.GetInstance<IConfiguration>();
			var key = config.GetValue("GoogleAnalyticsKey");

			return html.Raw(String.Format(Script, key));
		}
	}
}