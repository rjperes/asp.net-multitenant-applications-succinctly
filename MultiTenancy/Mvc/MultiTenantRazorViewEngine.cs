﻿using System;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using Common;

namespace Mvc
{
	public sealed class MultiTenantRazorViewEngine : RazorViewEngine
	{
		private Boolean pathsSet = false;

		public MultiTenantRazorViewEngine() : this(false)
		{
		}

		public MultiTenantRazorViewEngine(Boolean usePhysicalPathsPerTenant)
		{
			this.UsePhysicalPathsPerTenant = usePhysicalPathsPerTenant;
		}

		public Boolean UsePhysicalPathsPerTenant { get; private set; }

		private void SetPaths(ITenantConfiguration tenant)
		{
			if (this.UsePhysicalPathsPerTenant == true)
			{
				if (this.pathsSet == false)
				{
					this.ViewLocationFormats =
						new String[]
						{
							String.Concat("~/Views/", tenant.Name(), "/{1}/{0}.cshtml"),
							String.Concat("~/Views/", tenant.Name(), "/{1}/{0}.vbhtml")
						}
						.Concat(this.ViewLocationFormats).ToArray();
					this.pathsSet = true;
				}
			}
		}

		/*private ITenantConfiguration GetConfiguration()
		{
			var tenantIdentifier = DependencyResolver.Current.GetService<ITenantIdentifierStrategy>();
			var tenant = tenantIdentifier.GetCurrentTenant(HttpContext.Current.Request.RequestContext);
			return (this.GetConfiguration(tenant));
		}

		private ITenantConfiguration GetConfiguration(String tenant)
		{
			var tenantConfiguration = DependencyResolver.Current.GetServices<ITenantConfiguration>().SingleOrDefault(x => x.Name == tenant);
			this.SetPaths(tenantConfiguration);
			return (tenantConfiguration ?? GetConfiguration(TenantsConfiguration.DefaultTenant));
		}*/

		public override ViewEngineResult FindView(ControllerContext controllerContext, String viewName, String masterName, Boolean useCache)
		{
			var tenantConfiguration = TenantsConfiguration.GetCurrentTenant();
			/*var tenantConfiguration = this.GetConfiguration();*/
			var virtualPath = String.Format("~/{0}", tenantConfiguration.Name());
			var physicalPath = String.Format("~/Content/{0}", tenantConfiguration.Theme());

			if (BundleTable.Bundles.Any(b => b.Path == virtualPath) == false)
			{
				var bundle = new StyleBundle(virtualPath).IncludeDirectory(physicalPath, "*.css");
				BundleTable.Bundles.Add(bundle);
			}

			return (base.FindView(controllerContext, viewName, tenantConfiguration.MasterPage(), useCache));
		}
	}
}