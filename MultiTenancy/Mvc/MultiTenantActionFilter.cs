﻿using System;
using System.Web.Mvc;
using Common;

namespace Mvc
{
	public sealed class MultiTenantActionFilter : IActionFilter
	{
		void IActionFilter.OnActionExecuted(ActionExecutedContext filterContext)
		{
		}

		void IActionFilter.OnActionExecuting(ActionExecutingContext filterContext)
		{
			var tenant = TenantsConfiguration.GetCurrentTenant();
			filterContext.Controller.ViewBag.Tenant = tenant.Name();
			filterContext.Controller.ViewBag.TenantCSS = String.Concat("~/", filterContext.Controller.ViewBag.Tenant);
		}
	}
}