﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security;
using Common;

namespace WebForms
{
	[Serializable]
	public sealed class TenantPermission : IPermission
	{
		public TenantPermission(IEnumerable<String> tenants)
		{
			this.Tenants = tenants;
		}

		public IEnumerable<String> Tenants { get; private set; }

		#region IPermission Members

		public IPermission Copy()
		{
			return new TenantPermission(this.Tenants.ToArray());
		}

		public void Demand()
		{
			var tenant = TenantsConfiguration.GetCurrentTenant();

			if (this.Tenants.Any(t => tenant.Name() == t) == false)
			{
				throw new SecurityException("Current tenant is not allowed to access this resource.");
			}
		}

		public IPermission Intersect(IPermission target)
		{
			var other = target as TenantPermission;

			if (other == null)
			{
				throw new ArgumentException("Invalid permission.", "target");
			}

			return new TenantPermission(this.Tenants.Intersect(other.Tenants).ToArray());
		}

		public Boolean IsSubsetOf(IPermission target)
		{
			var other = target as TenantPermission;

			if (other == null)
			{
				throw new ArgumentException("Invalid permission.", "target");
			}

			return this.Tenants.All(t => other.Tenants.Contains(t));
		}

		public IPermission Union(IPermission target)
		{
			var other = target as TenantPermission;

			if (other == null)
			{
				throw new ArgumentException("Invalid permission.", "target");
			}

			return new TenantPermission(this.Tenants.Concat(other.Tenants).Distinct().ToArray());
		}

		#endregion

		#region ISecurityEncodable Members

		public void FromXml(SecurityElement e)
		{
			if (e == null)
			{
				throw new ArgumentNullException("e");
			}

			var tenantTag = e.SearchForChildByTag("Tenants");

			if (tenantTag == null)
			{
				throw new ArgumentException("Element does not contain any tenants.", "e");
			}

			var tenants = tenantTag.Text.Split(',').Select(t => t.Trim());

			this.Tenants = tenants;
		}

		public SecurityElement ToXml()
		{
			var xml = String.Concat("<IPermission class=\"", this.GetType().AssemblyQualifiedName, "\" version=\"1\" unrestricted=\"false\"><Tenants>", String.Join(", ", this.Tenants), "</Tenants></IPermission>");
			return SecurityElement.FromString(xml);
		}

		#endregion
	}
}
