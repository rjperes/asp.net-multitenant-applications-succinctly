﻿<%@ Page Language="C#" MasterPageFile="~/AbcCom.Master" CodeBehind="Default.aspx.cs" Inherits="WebForms.Default" Trace="true" %>
<asp:Content ContentPlaceHolderID="head" runat="server">
	Head content goes here

	<script type="text/javascript">// <![CDATA[
		(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
			(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
			m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
			})(window,document,'script','//www.google-analytics.com/analytics.js','ga');
			ga('create', '{0}', '{1}');
			ga('send', 'pageview');
	// ]]></script>

</asp:Content>

<asp:Content ContentPlaceHolderID="body" runat="server">
	Body content goes here

	<asp:Label runat="server" Text="abc.com only" Visible="<%$ MatchTenant:abc.com %>"/>

	<asp:TextBox runat="server" TextMode="MultiLine" />

	<asp:TextBox runat="server" ID="email" SkinID="Email" />

	<asp:Button runat="server" SkinID="Dark" Text="Click Me" />

	<asp:Button runat="server" SkinID="Light" Text="Click Me" />

	<asp:Image runat="server" ImageUrl="~/bla.png" />

	<asp:TextBox runat="server" />

	<asp:Image runat="server" ImageUrl="<%$ ThemeFileUrl:/logo.jpg %>" />


	<asp:DropDownList runat="server" SelectMethod="GetItems" OnCreatingModelDataSource="Unnamed_CreatingModelDataSource" OnCallingDataMethods="Unnamed_CallingDataMethods" />


</asp:Content>
