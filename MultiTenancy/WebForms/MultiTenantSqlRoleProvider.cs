﻿using System;
using System.Web.Security;
using Common;

namespace WebForms
{
	public class MultiTenantSqlRoleProvider : SqlRoleProvider
	{
		public override String ApplicationName
		{
			get
			{
				return (TenantsConfiguration.GetCurrentTenant().Name());
			}
			set
			{
			}
		}
	}
}