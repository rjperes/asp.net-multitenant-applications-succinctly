﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Web.UI;
using Common;
using Microsoft.Practices.ServiceLocation;

namespace WebForms
{
	public partial class Default : Page
	{
		protected override void OnLoad(EventArgs e)
		{
			this.Trace.Write("On Page.Load");

			base.OnLoad(e);
		}

		protected void Page_Load(object sender, EventArgs e)
		{

			var log = ServiceLocator.Current.GetInstance<ILog>();
			log.Write("Bla", 0, TraceEventType.Information);
		}

		public IQueryable<String> GetItems()
		{
			return new[] { "A", "B", "C" }.AsQueryable();
		}

		protected void Unnamed_CreatingModelDataSource(object sender, System.Web.UI.WebControls.CreatingModelDataSourceEventArgs e)
		{

		}

		protected void Unnamed_CallingDataMethods(object sender, System.Web.UI.WebControls.CallingDataMethodsEventArgs e)
		{

		}
	}
}