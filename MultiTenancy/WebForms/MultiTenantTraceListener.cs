﻿using System;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Web;
using Common;

namespace WebForms
{
	[Serializable]
	public sealed class NewRowEventArgs : EventArgs
	{
		public NewRowEventArgs(DataRow newRow)
		{
			this.NewRow = newRow;
		}

		public DataRow NewRow { get; private set; }
	}

	public sealed class MultiTenantTraceListener : WebPageTraceListener
	{
		//EventLogTraceListener (Event Log)
		//WebPageTraceListener (Trace)
		//IisTraceListener (IIS)
		//EventProviderTraceListener (ETW)
		//ConsoleTraceListener (Console)
		//DefaultTraceListener (File / Debug)
		//TextWriterTraceListener (File)
		//DelimitedListTraceListener (File)
		//EventSchemaTraceListener (File)
		//XmlWriterTraceListener (File)
		//FileLogTraceListener (File)

		private static readonly MethodInfo GetDataMethod = typeof(TraceContext).GetMethod("GetData", BindingFlags.NonPublic | BindingFlags.Instance);

		public static event EventHandler<NewRowEventArgs> NewRow;

		public void OnNewRow(NewRowEventArgs e)
		{
			var handler = NewRow;

			if (handler != null)
			{
				handler(this, e);
			}
		}

		public static void Register()
		{
			if (Trace.Listeners.OfType<MultiTenantTraceListener>().Any() == false)
			{
				Trace.Listeners.Add(new MultiTenantTraceListener());
			}
		}

		public override void WriteLine(String message, String category)
		{
			var ds = GetDataMethod.Invoke(HttpContext.Current.Trace, null) as DataSet;
			var dt = ds.Tables["Trace_Trace_Information"];
			var dr = dt.Rows[dt.Rows.Count - 1];

			this.OnNewRow(new NewRowEventArgs(dr));

			dr["Trace_Message"] = String.Concat(TenantsConfiguration.GetCurrentTenant().Name(), ": ", dr["Trace_Message"]);

			base.WriteLine(message, category);
		}
	}
}