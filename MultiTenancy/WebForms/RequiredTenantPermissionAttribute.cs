﻿using System;
using System.Linq;
using System.Security;
using System.Security.Permissions;

namespace WebForms
{
	[Serializable]
	[AttributeUsage(AttributeTargets.Method | AttributeTargets.Class, AllowMultiple = true, Inherited = false)]
	public sealed class RequiredTenantPermissionAttribute : CodeAccessSecurityAttribute
	{
		public RequiredTenantPermissionAttribute(SecurityAction action) : base(action)
		{
		}

		public override IPermission CreatePermission()
		{
			return new TenantPermission(this.Tenants.Split(',').Select(t => t.Trim()).ToArray());

		}

		public String Tenants { get; set; }
	}
}