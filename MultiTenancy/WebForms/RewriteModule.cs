﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebForms
{
	public class RewriteModule : IHttpModule
	{
		public void Dispose()
		{
			
		}

		public void Init(HttpApplication context)
		{
			context.BeginRequest += this.OnBeginRequest;
		}

		private void OnBeginRequest(object sender, EventArgs e)
		{
			var app = sender as HttpApplication;
	
			if (app.Request.PathInfo == "/abc.com")
			{
				app.Context.RewritePath("Default.aspx");
			}
		}
	}
}