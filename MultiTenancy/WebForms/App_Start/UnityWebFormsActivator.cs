using Microsoft.Practices.ServiceLocation;
using Microsoft.Practices.Unity;
using Microsoft.Practices.Unity.Mvc;
using Microsoft.Web.Infrastructure.DynamicModuleHelper;

[assembly: WebActivatorEx.PreApplicationStartMethod(typeof(WebForms.App_Start.UnityWebActivator), "Start")]
[assembly: WebActivatorEx.ApplicationShutdownMethod(typeof(WebForms.App_Start.UnityWebActivator), "Shutdown")]

namespace WebForms.App_Start
{
	/// <summary>Provides the bootstrapping for integrating Unity with ASP.NET MVC.</summary>
	public static class UnityWebActivator
	{
		/// <summary>Integrates Unity when the application starts.</summary>
		public static void Start()
		{
			var container = UnityConfig.GetConfiguredContainer();

			ServiceLocator.SetLocatorProvider(() => new UnityServiceLocator(container));

			// TODO: Uncomment if you want to use PerRequestLifetimeManager
			DynamicModuleUtility.RegisterModule(typeof(UnityPerRequestHttpModule));
		}

		/// <summary>Disposes the Unity container when the application is shut down.</summary>
		public static void Shutdown()
		{
			var container = UnityConfig.GetConfiguredContainer();
			container.Dispose();
		}
	}
}