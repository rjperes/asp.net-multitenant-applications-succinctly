﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using Common;

namespace WebForms
{
	public class MultiTenantPageHandlerFactory : PageHandlerFactory
	{
		public override IHttpHandler GetHandler(HttpContext context, String requestType, String virtualPath, String path)
		{
			var tenant = TenantsConfiguration.GetCurrentTenant();

			var page = base.GetHandler(context, requestType, virtualPath, path) as Page;
			page.Theme = tenant.Theme();

			return page;
		}
	}
}