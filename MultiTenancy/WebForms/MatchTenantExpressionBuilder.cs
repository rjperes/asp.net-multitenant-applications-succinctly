﻿using System;
using System.CodeDom;
using System.Web.Compilation;
using System.Web.UI;
using Common;

namespace WebForms
{
	[ExpressionPrefix("MatchTenant")]
	public sealed class MatchTenantExpressionBuilder : ExpressionBuilder
	{
		public override Object EvaluateExpression(Object target, BoundPropertyEntry entry, Object parsedData, ExpressionBuilderContext context)
		{
			if (String.IsNullOrWhiteSpace(entry.Expression) == true)
			{
				return (base.EvaluateExpression(target, entry, parsedData, context));
			}
			else
			{
				return (MatchTenant(entry.Expression));
			}
		}

		public override Boolean SupportsEvaluate
		{
			get
			{
				return (true);
			}
		}

		public override CodeExpression GetCodeExpression(BoundPropertyEntry entry, Object parsedData, ExpressionBuilderContext context)
		{
			if (String.IsNullOrWhiteSpace(entry.Expression) == true)
			{
				return (new CodePrimitiveExpression(String.Empty));
			}
			else
			{
				return (new CodeMethodInvokeExpression(new CodeMethodReferenceExpression(new CodeTypeReferenceExpression(this.GetType()), "MatchTenant"), new CodePrimitiveExpression(entry.Expression)));
			}
		}

		public static Boolean MatchTenant(String tenant)
		{
			var currentTenant = TenantsConfiguration.GetCurrentTenant();

			if (tenant == currentTenant.Name())
			{
				return (true);
			}

			if (tenant.StartsWith("!") == true)
			{
				if (tenant.Substring(1) != currentTenant.Name())
				{
					return (false);
				}
			}

			return (false);
		}
	}
}