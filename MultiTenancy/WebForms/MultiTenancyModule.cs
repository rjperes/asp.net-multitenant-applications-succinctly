﻿using System;
using System.Web;
using System.Web.UI;
using Common;
using Microsoft.Web.Infrastructure.DynamicModuleHelper;
using WebForms;

[assembly: WebActivatorEx.PreApplicationStartMethod(typeof(MultiTenancyModule), "Register")]

namespace WebForms
{
	public sealed class MultiTenancyModule : IHttpModule
	{
		#region IHttpModule Members

		public void Dispose()
		{
		}

		public void Init(HttpApplication context)
		{
			context.PreRequestHandlerExecute += this.OnPreRequestHandlerExecute;
		}

		#endregion

		public static String MasterPagesPath { get; set; }

		public static void Register()
		{
			DynamicModuleUtility.RegisterModule(typeof(MultiTenancyModule));
		}

		private void OnPreRequestHandlerExecute(Object sender, EventArgs e)
		{
			var app = sender as HttpApplication;
			var tenant = TenantsConfiguration.GetCurrentTenant();

			if ((app != null) && (app.Context != null))
			{
				var page = app.Context.CurrentHandler as Page;

				if (page != null)
				{
					page.PreInit += (s, args) =>
					{
						var p = s as Page;

						if (String.IsNullOrWhiteSpace(tenant.MasterPage()) == false)
						{
							p.MasterPageFile = String.Format("{0}/{1}.Master", MasterPagesPath, tenant.MasterPage());
						}

						if (String.IsNullOrWhiteSpace(tenant.Theme()) == false)
						{
							p.Theme = tenant.Theme();
						}
					};
				}
			}
		}
	}
}