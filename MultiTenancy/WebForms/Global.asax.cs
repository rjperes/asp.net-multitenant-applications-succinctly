﻿using System;
using System.Diagnostics;
using System.Security.Permissions;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Instrumentation;
using System.Web.Management;
using System.Web.Routing;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.Adapters;
using Common;
using Microsoft.Practices.EnterpriseLibrary.Logging.Database;
using Microsoft.Practices.ServiceLocation;
using Microsoft.Samples.Eventing;

//[assembly:PreApplicationStartMethod(typeof(WebForms.Global), "Start")]

namespace WebForms
{
	class CustomPageExecutionListener : PageExecutionListener
	{

		public override void BeginContext(PageExecutionContext context)
		{
		}

		public override void EndContext(PageExecutionContext context)
		{
		}
	}


	public class WebFormsRouteHandler : PageRouteHandler
	{
		public WebFormsRouteHandler(String virtualPath) : base(virtualPath)
		{
		}

		public override IHttpHandler GetHttpHandler(RequestContext requestContext)
		{
			var tenant = TenantsConfiguration.GetCurrentTenant();

			return base.GetHttpHandler(requestContext);
		}
	}

	public class Rule
	{

	}

	class CustomTraceListener : TraceListener
	{

		public override void Write(string message)
		{

		}

		public override void WriteLine(string message)
		{

		}
	}

	public class TesteControlAdapter : WebControlAdapter
	{
		protected override void OnLoad(EventArgs e)
		{
			base.OnLoad(e);
		}
	}

	public class Global : HttpApplication
	{
		public static void Start()
		{
			var customRoute = new Route(String.Empty, new WebFormsRouteHandler("~/Default.aspx"));
			RouteTable.Routes.Add(customRoute);
		}

		[RequiredTenantPermission(SecurityAction.Demand, Tenants = "aaa, bbb")]
		public void Teste()
		{
		}

		protected void Application_Start(Object sender, EventArgs e)
		{
			return;
			var prov = MultiTenantEventProvider.FindProvider("abc.com");
			//do
			//MultiTenantEventProvider.RegisterEvent((s, e) => { });


			Start();

			Trace.Listeners.Add(new CustomTraceListener());

			TenantsConfiguration.Initialize();
		}

		private void Run()
		{
	        var RewriteProviderId = new Guid(MultiTenantEventSource.SourceId);

			using (var watcher = new EventTraceWatcher(MultiTenantEventSource.SourceName, new Guid(MultiTenantEventSource.SourceId)))
			{
				watcher.EventArrived += (sender, e) =>
				{
					Console.WriteLine();
				};

				watcher.Start();

				Console.ReadLine();
			}
		}

		protected void Application_BeginRequest(Object sender, EventArgs e)
		{
			var adps = HttpContext.Current.Request.Browser.Adapters;
			adps[typeof(TextBox).AssemblyQualifiedName] = typeof(TesteControlAdapter).AssemblyQualifiedName;

			return;
			Task.Run(() =>
				{
					Run();
				});


			//var authorized = UrlAuthorizationModule.CheckUrlAccessForPrincipal(this.Context.Request.Path, this.Context.User, this.Context.Request.HttpMethod);

			PageInstrumentationService.IsEnabled = true;
			HttpContext.Current.PageInstrumentation.ExecutionListeners.Add(new CustomPageExecutionListener());

			Thread.CurrentThread.Rename(TenantsConfiguration.GetCurrentTenant().Name());

			var diags = ServiceLocator.Current.GetInstance<IDiagnostics>();
			diags.RaiseEtwEvent("bla", 1000);

			var config = ServiceLocator.Current.GetInstance<IConfiguration>();
			var key = config.GetValue("GoogleAnalyticsKey");
		}
	}
}