﻿using System.Web.UI;
using Common;

namespace WebForms
{
	public abstract class MultiTenantPage : Page
	{
		public ITenantConfiguration CurrentTenant
		{
			get { return (TenantsConfiguration.GetCurrentTenant()); }
		}

		public IDiagnostics Diagnostics
		{
			get { return (TenantsConfiguration.Services.Diagnostics); }
		}

		public new ICache Cache
		{
			get { return (TenantsConfiguration.Services.Cache); }
		}

		public ILog Log
		{
			get { return (TenantsConfiguration.Services.Log); }
		}
		public IConfiguration Configuration
		{
			get { return (TenantsConfiguration.Services.Configuration); }
		}
	}
}